<?php
/*
Plugin Name: Naap Seattle buy ticket
Plugin URI: #
Description: Buy ticket and order by Paypal to become a member of Naap.
Version: 1.0
Author: Anomyous
Author URI: #
*/

/**
 * Register a custom menu page.
 */
$file = __FILE__;
$pluginURL = plugin_dir_url($file);

function css_and_js() {
  wp_register_style('css_and_js', plugins_url('css/style.css',__FILE__ ));
  wp_enqueue_style('css_and_js');
  wp_register_script( 'css_and_js', plugins_url('js/jquery-1.7.1.min.js',__FILE__ ));
  wp_enqueue_script('css_and_js');
}
add_action( 'admin_init','css_and_js');

 add_action('admin_menu', 'plugin_setup_menu_naap_membership');

 function plugin_setup_menu_naap_membership(){
         add_menu_page( 'Buy ticket Page', 'Buy ticket', 'manage_options', 'Naap-membership', 'test_init', 'dashicons-cart' );
 }

 function test_init()
 {
    global $wpdb;
    $results = $wpdb->get_results( "SELECT * FROM naap_memberships_users order by Time DESC");
    $row_per_page=15;
    $rows=count($results);
    if ($rows>$row_per_page) $page=ceil($rows/$row_per_page);
    else $page=1;
    if(isset($_GET['start']) && (int)$_GET['start'])
      $start=$_GET['start'];
    else
      $start=0;
    $results = $wpdb->get_results( "SELECT * FROM naap_memberships_users order by Time DESC limit $start,$row_per_page ");



?>
<h1 style="
    font-size: 23px;
    font-weight: 400;
    margin: 0;
    padding: 9px 15px 4px 0;
    line-height: 29px;">Member buy ticket list
</h1>
<table class="wp-list-table widefat fixed striped users">
  <thead>
    <tr>
      <td id="cb" class="manage-column column-cb check-column">
        <label class="screen-reader-text" for="cb-select-all-1">Select All</label>
        <input id="cb-select-all-1" type="checkbox">
      </td>
      <th scope="col" id="firstname" class="manage-column column-username column-primary sortable desc">
        <a href="#"><span>Username</span><span class="sorting-indicator"></span></a>
      </th>
      <th scope="col" id="firstname" class="manage-column column-username column-primary sortable desc">
        <a href="#"><span>First Name</span><span class="sorting-indicator"></span></a>
      </th>
      <th scope="col" id="lastname" class="manage-column column-username column-primary sortable desc">
        <a href="#"><span>Last Name</span><span class="sorting-indicator"></span></a>
      </th>
      <th scope="col" id="email" class="manage-column column-username column-primary sortable desc">
        <a href="#"><span>Email</span><span class="sorting-indicator"></span></a>
      </th>
      <th scope="col" id="company" class="manage-column column-username column-primary sortable desc">
        <a href="#"><span>Company</span><span class="sorting-indicator"></span></a>
      </th>
      <th scope="col" id="number" class="manage-column column-username column-primary sortable desc">
        <a href="#"><span>Number of ticket</span><span class="sorting-indicator"></span></a>
      </th>
      <th scope="col" id="type" class="manage-column column-username column-primary sortable desc">
        <a href="#"><span>Total Amount</span><span class="sorting-indicator"></span></a>
      </th>
      <th scope="col" id="type" class="manage-column column-username column-primary sortable desc">
        <a href="#"><span>Discount Code</span><span class="sorting-indicator"></span></a>
      </th>
      <th scope="col" id="type" class="manage-column column-username column-primary sortable desc">
        <a href="#"><span>Date</span><span class="sorting-indicator"></span></a>
      </th>
    </tr>
  </thead>
  <tbody id="the-list" data-wp-lists="list:user">
<?php
    foreach($results as $row)
    {
?>
<tr id="user-4">
    <th scope="row" class="check-column">
      <label class="screen-reader-text" for="user_4">Select</label>
      <input name="users[]" id="user_4" class="none" value="4" type="checkbox">
    </th>
    <td class="username column-username has-row-actions column-primary" data-colname="username">
      <?php if(($row->iduser)=="") echo "Guest"; else echo $row->iduser ?>
    </td>
   <td class="username column-username has-row-actions column-primary" data-colname="firstname">
     <?php echo $row->firstname ?>
   </td>
   <td class="username column-username has-row-actions column-primary" data-colname="lastname">
     <?php echo $row->lastname ?>
   </td>
   <td class="username column-username has-row-actions column-primary" data-colname="email">
     <?php echo $row->email ?>
   </td>
   <td class="username column-username has-row-actions column-primary" data-colname="number">
     <?php echo $row->company ?>
   </td>
   <td class="username column-username has-row-actions column-primary" data-colname="number">
     <?php echo $row->number ?>
   </td>
   <td class="username column-username has-row-actions column-primary" data-colname="type">
     <?php echo $row->totalamount ?>
   </td>
   <td class="username column-username has-row-actions column-primary" data-colname="type">
     <?php echo $row->discountcode ?>
   </td>
   <td class="username column-username has-row-actions column-primary" data-colname="type">
     <?php echo $row->Time ?>
   </td>
 </tr>
<?php
    }
?>
</table>
<div class="tablenav-pages" style="font-size:14px"><br/><span class="displaying-num"><?php echo $rows ?> items</span><br/>
<?php
        $page_cr=($start/$row_per_page)+1;
        for($i=1;$i<=$page;$i++)
        {
         if ($page_cr!=$i) echo "<div class='phantrang'><a href='admin.php?page=Naap-membership&start=".$row_per_page*($i-1)."'>$i</a></div>";
         else echo "<div class='phantrang'>".$i." "."</div>";
        }
 ?>
</div>
<?php
 }
 ?>
