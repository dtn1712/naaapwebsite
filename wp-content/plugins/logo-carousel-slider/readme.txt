﻿=== Logo Carousel Slider ===
Contributors: AdlPlugins
Tags: logo, logo carousel, logo slider, logo carousel slider, logo showcase, logo slideshow, clients logo slider, sponsors logo slider, logos, logo slide, carousel, carousel slider, image carousel, image slider, clients logos show, sponsors, clients, partners
Requires at least: 3.5
Tested up to: 4.4.2
Stable tag: 1.2
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

It allows you to easily create logo carousel slider to display logos of clients, partners, sponsors, affiliates etc.

== Description ==

Logo Carousel Slider is a plugin to display logos of clients, partners, sponsors, affiliates etc. in a beautiful carousel slider. Its fully mobile and tablet devices responsive. Using shortcode [logo_carousel_slider], you can easily display it anywhere of your website.  

= Features of this plugin =
*   100% Responsive and mobile friendly.
*   Touch and Swipe enabled so works great on devices like iPhone, iPad, Blackberry, Android etc.
*   Support all modern browsers.
*   Very lightweight.
*   Enternal/external logo linking.
*   Display logo with or without title.
*   Display logo with or without border around.
*   Settings panel.
*   Control number of logos to be displayed.
*   Unlimited logos display.
*   Auto logos resize and crop.
*   Autoplay control.
*   Navigation arrows.
*   Logo hover effect enable/disable.
*   Pagination.
*   Translation ready.
*   RTL support.
*   Many more…

Live demo: <http://demos.adlplugins.com/logo-carousel-slider>

Upgrade to [__Pro version__](http://adlplugins.com/plugin/logo-carousel-slider-pro) to get more amazing features of the plugin. 

= Features of the Pro version =
*   It has all the features of the free version.
*   Advanced shortcode generator. No more pain of understanding shortcode attributes and write them.
*   Advanced settings panel.
*   Unique settings for each logo carousel slider.
*   Unlimited logo carousel sliders with differnt logos.
*   Multiple logo carousel sliders on one page.
*   Logos categorization.
*   Logos displaying from latest and older published, category, logo id, specific month and year.
*   Control number of logos to display on desktop, tablet and mobile devices.
*   Two different styles for navigation arrows.
*   Two different styles for pagination.
*   Tooltip.
*   Slide Speed control.
*   Mouse over stop option.
*   Super lightweight.
*   Slider title font size, color, hover color change options.
*   Navigation arrows, background, hover color change options.
*   Logo border color and hover color change options.
*   Logo title font size, color, hover color change options.
*   Tooltip font size, color, background color, hover color change options.
*   Pagination color change option.
*   100% Responsive and mobile friendly.
*   Touch and Swipe enabled so works great on devices like iPhone, iPad, Blackberry, Android etc.
*   Support all modern browsers.
*   Many more…

Live demo: <http://demos.adlplugins.com/logo-carousel-slider-pro-demos>


== Installation ==

1. Unzip the downloaded zip file and upload the plugin folder into the `wp-content/plugins/` directory. Alternatively, upload from Plugins >> Add New >> Upload Plugin.
2. Activate the plugin from Plugins page.

= Usage =

After successfully installing and activating the plugin, you will find "Logo Carousel" menu on left column of WordPress dashboard. To add a logo, go to Logo Carousel >> Add New Logo. Configure any options as desired using "Settings" page. Then use shortcode [logo_carousel_slider slider_title="Slider Title"] to display the logo carousel slider. If you don't want to display slider title/name, just use [logo_carousel_slider].

== Screenshots ==
1. Settings Page
2. Demos
3. Pro version's demos

== Changelog ==

= 1.2 =
* Fixed few issues and made some minor updates
= 1.1 =
* Added logo hover effect enable and disable feature
= 1.0 =
* Initial release
