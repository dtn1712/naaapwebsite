/*! 
 * Facebook Events Calendar WordPress Plugin 
 * @author: WPPress
 */
var momentz = function(datetime) {
    if (typeof WPPress.FBCalTimezone != "undefined" && typeof moment != "undefined") {
        return moment(datetime).tz(WPPress.FBCalTimezone);
    } else {
        return moment(datetime);
    }
}
if (typeof moment.locale != "undefined" && typeof WPPress.calendar.lang != "undefined") {
    moment.locale(WPPress.calendar.lang, WPPress.calendar.locale);
}

jQuery(document).ready(function($) {
    _.templateSettings = {
        escape: /<%-([\s\S]+?)%>/g,
        evaluate: /<%([\s\S]+?)%>/g,
        interpolate: /<%=([\s\S]+?)%>/g
    };
    var popupIn = "zoomIn",
        popupOut = "zoomOut";

    WPPress._initFB_calendar = function() {
        var _body = $('body');
        var _window = $(window);

        $('#facebook-events-popup').remove();
        var _popup = $('<div>').attr({
                id: 'facebook-events-popup'
            }).addClass('facebook-events-popup animated'),
            _closeBtn = $('<div>', {
                class: 'close-popup'
            }).append($('<img />', {
                src: WPPress.facebooEventsCalendar + 'images/closebtn.svg',
                title: 'Close',
                class: 'close-popup-btn'
            })),
            _popupBody = $('<div>', {
                class: 'modal-content'
            });
        _body.append(_popup);
        _popup.append(_closeBtn).append(_popupBody);
        _popup.on('click', '.close-popup-btn', function(e) {
            e.preventDefault();
            $('body, html').css({
                'overflow': 'auto'
            });
            _popup.removeClass(popupIn).addClass(popupOut);
            setTimeout(function() {
                _popup.hide();
            }, 800);

        });
        $('.fb-event-calendar').each(function() {
            var _this = $(this);
            var _id = _this.attr('data-user-id');
            var _template = _this.attr('data-template');
            var _include_attending = _this.attr('data-include-attending');
            var _hide_old = _this.attr('data-hide-old');
            var _show_popup = _this.attr('data-show-in-popup');
            var _loadedEvents = [];
            var _getEvents = function(year, month) {
                    if (!_.contains(_loadedEvents, year + "-" + month)) {
                        $('.event-listing', _this).addClass('loading');
                        var params = {
                            action: "get_fb_cal_event",
                            id: _id,
                            attending: _include_attending,
                            hide_old: _hide_old,
                            year: year,
                            month: month,
                        };
                        $.getJSON(WPPress.adminajax, params, function(data) {
                            _loadedEvents.push(year + "-" + month);
                            _calendar.addEvents(data);
                            $('.event-listing', _this).removeClass('loading');
                            _window.trigger('resize');

                        });
                    }
                },
                _calendar = _this.clndr({
                    template: $('#' + _template).html(),
                    mode: "daily",
                    weekOffset: WPPress.weekoffset,
                    daysOfTheWeek: WPPress.calendar.locale.weekdaysMin,
                    clickEvents: {
                        onMonthChange: function(data) {
                            var _year = moment(data._d).format("YYYY"),
                                _month = moment(data._d).format("MM");
                            _getEvents(_year, _month);
                        }
                    }
                });
            _this.on('click', '.day[data-date]', function(e) {
                if (!$(this).hasClass('event') || $(this).hasClass('active-filter')) {
                    $('.event-listing .event-item', _this).removeClass('hidden-event');
                    $('.day[data-date]', _this).removeClass('faded').removeClass('active-filter');
                } else {
                    var _activeDate = $(this).attr('data-date');
                    $(this).addClass('active-filter');
                    $('.event-listing .event-item', _this).addClass('hidden-event');
                    $(this).removeClass('faded').siblings('.day[data-date]').addClass('faded').removeClass('active-filter');
                    $('.event-listing .event-item-' + _activeDate, _this).removeClass('hidden-event');
                }
            });
            if (_show_popup == "yes") {
                _this.on('click', 'a[href*="facebook.com/events"]', function(e) {
                    e.preventDefault();
                    $('body, html').css({
                        'overflow': 'hidden'
                    });
                    var _event = $(this).closest('.event-item');
                    var _popup_content = $('.event-popup', _event).html();
                    _popupBody.empty().append(_popup_content);
                    _popup.removeClass(popupOut).addClass(popupIn).show();
                    if ($('.event-in-map', _popupBody).length > 0) {
                        $('.event-in-map', _popupBody).each(function() {
                            var _map_latlng = $(this).attr('data-latlng'),
                                _src = 'https://www.google.com/maps/embed/v1/search?' + $.param({
                                    key: "AIzaSyCLs2XM2DUYl0TubwVzAuUnftr3c991zWg",
                                    q: _map_latlng,
                                    zoom: 19
                                });
                            var _map_frame = $('<iframe />', {
                                border: 0,
                                src: _src
                            });
                            $(this).empty().append(_map_frame);
                        });
                    }
                    $('a[href*="http"]', _popupBody).attr({
                        target: "_blank"
                    });
                    _popup.scrollTop(0);
                });
            }
            _getEvents(moment().format("YYYY"), moment().format("MM"));
        });
    };
    WPPress._initFB_calendar();
});
