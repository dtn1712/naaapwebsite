<div class="fb-event-calendar fb-calendar-list-view" <?php echo join( " ", $params); ?>></div>
<script type="text/template" id="fb-calendar-template-<?php
echo $calendarID; ?>">
    <div class="clndr-controls">
        <div class="current-month">
            {{month}}, {{year}}
            <div class="clndr-controls-wrap">
                <div class="clndr-previous-button"><i class="fbcal-arrow-left"></i>
                </div>
                <div class="clndr-next-button"><i class="fbcal-arrow-right"></i>
                </div>
            </div>
        </div>
    </div>
    {% if(eventsThisMonth.length>0){ %}
    <div class="event-listing">
        <div class="event-listing-title">
            <?php _e( 'EVENTS THIS MONTH', 'facebook-events'); ?>
        </div>
        {% _.each(eventsThisMonth, function(event) { %}
        <div class="event-item clearfix">
            <?php if($show_event_cover=="yes" ){ ?> {% if(event.cover){ %}
            <div class="event-item-cover">
                <a href="http://facebook.com/events/{{event.id}}" target="_blank"> <img src="{{ event.cover}}" alt="{{event.title}}" /></a>
            </div>
            {% } %}
            <?php } ?>
            <div class="event-item-detail">
                <div class="event-item-name">
                    <a href="https://facebook.com/events/{{event.id}}" target="_blank">{{event.title}}</a>
                </div>
                <?php if($show_event_date=="yes" ){ ?>
                <div class="event-item-time">
                    <strong><i class="fbcal-calendar"></i> <?php _ex('Date:','Event Date','facebook-events');?></strong> {{momentz(event.date).format( 'MMMM Do')}} {% if(!event.all_day_event){ %}
                    <?php _ex( 'at', '`at` eg: 29th December at 12:42 AM', 'facebook-events'); ?> {{momentz(event.date).format( 'h:mm a z \(\U\T\CZ\)')}} {% } %}
                </div>
                <?php } ?>
                <?php if($show_event_venue=="yes" || $show_event_map=="yes" ){ ?>
                <div class="event-item-location">
                    {% if(event.location){ %}<strong> <i class="fbcal-location"></i> <?php _ex('Venue:','Location','facebook-events'); ?></strong> {{event.location}} {% } %}
                    <?php if($show_event_map=="yes" ){ ?> {% if(typeof event.venue.latitude !="undefined" && typeof event.venue.longitude !="undefined"){ %} -
                    <a href="http://www.google.com/maps?q={{ event.venue.latitude}},{{ event.venue.longitude}}" target="_blank" class="event-item-map-location">
                        <?php _e( 'Show In Map', 'facebook-events'); ?>
                    </a>
                    {% } %}
                    <?php }?>
                </div>
                <?php } ?>
               
            </div>
            <?php if($show_in_popup=="yes" ){ ?>
            <div class="event-popup">
                {% if(event.cover){ %}
                <div class="event-cover">
                    <a href="https://facebook.com/events/{{event.id}}" target="_blank">
                        <img src="{{event.cover}}" alt="{{event.title}}" />
                        
                        </a>
                </div>
                {% } %}
                <div class="event-title">
                    <h2>
                        <a href="https://facebook.com/events/{{event.id}}" target="_blank">
                            {{event.title}}</a></h2>
                    <div class="event-meta">{{momentz(event.date).format( 'MMMM Do')}} {% if(!event.all_day_event){ %}
                        <?php _ex( 'at', '`at` eg: 29th December at 12:42 AM', 'facebook-events'); ?> {{momentz(event.date).format('h:mm a \(\U\T\CZ\)')}} {% } %}</div>
                    {% if(event.owner){ %}
                    <div class="event-meta">
                        <?php _e( 'Hosted By:', 'facebook-events'); ?> <a href="http://facebook.com/profile.php?id={{event.owner.id}}" target="_blank">{{event.owner.name}}</a></div>
                    {% } %}
                </div>
                <div class="event-description">
                    {{event.description}}
                </div>
                <?php if($show_event_map=="yes" ){ ?> {% if( event.venue.latitude !="undefined" && typeof event.venue.longitude !="undefined"){ %}
                <div class="event-in-map" data-latlng="{{event.venue.latitude}},{{event.venue.longitude}}" data-venue="{{event.venue.street}}">
                </div>
                {% } %}
                <?php } ?>
            </div>
            <?php } ?>
        </div>
        {% }); %}
    </div>
    {% }else{ %}
    <div class="event-listing">
        <div class="event-listing-title">
            <?php _e( 'NO EVENTS THIS MONTH', 'facebook-events'); ?>
        </div>
    </div>
    {% } %}
</script>
