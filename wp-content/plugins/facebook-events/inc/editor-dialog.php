<?php wp_enqueue_script( 'jquery'); wp_enqueue_style( 'colors'); wp_enqueue_style( 'buttons'); wp_enqueue_style( 'ie'); wp_enqueue_style( 'wp-auth-check'); wp_enqueue_style( 'admin-bar'); wp_enqueue_style( 'fbcal-tinymce-popup'); global $wp_scripts; ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <title>
        <?php _e( 'Event Calendar Shortcode Generator', 'facebook-events'); ?>
    </title>
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <?php do_action( 'admin_print_scripts'); ?>
    <?php do_action( 'admin_print_styles'); ?>
    <script language="javascript" type="text/javascript" src="<?php
echo site_url(); ?>/wp-includes/js/tinymce/tiny_mce_popup.js"></script>
    <script language="javascript" type="text/javascript" src="<?php
echo site_url(); ?>/wp-includes/js/tinymce/utils/form_utils.js"></script>
    <base target="_self" />
    <script language="javascript" type="text/javascript">
    jQuery(document).ready(function($) {
        var _self = tinyMCEPopup;
        $('#create_shortcode').on('click', function(e) {
            e.preventDefault();
            var _fbid = $('#page_id').val(),
                _include_attending = $('#include_attending').is(":checked") ? true : false,
                _show_calendar = $('#show_event_calendar').is(":checked") ? true : false,
                _show_in_popup = $('#show_in_popup').is(":checked") ? true : false,
                _show_cover = $('#show_event_cover').is(":checked") ? true : false,
                _show_date = $('#show_event_date').is(":checked") ? true : false,
                _show_venue = $('#show_event_venue').is(":checked") ? true : false,
                _show_map = $('#show_event_map').is(":checked") ? true : false,
                _hide_old = $('#hide_past_events').is(":checked") ? true : false,
                _mode = $('#mode').val(),
                _args = [];
            var tag = "[fb-cal /]";
            if (_fbid.length > 0) {
                _args.push("id=\"" + _fbid + "\"");
            }
            if (!_show_calendar) {
                _args.push("show_event_calendar=\"no\"");
            }
            if (_mode != "calendar") {
                _args.push("mode=\"" + _mode + "\"");
            }
            if (_hide_old) {
                _args.push("hide_past_events=\"yes\"");
            }
            if (_include_attending) {
                _args.push("include_attending=\"yes\"");
            }
            if (!_show_cover) {
                _args.push("show_event_cover=\"no\"");
            }
            if (!_show_in_popup) {
                _args.push("show_in_popup=\"no\"");
            }
            if (!_show_date) {
                _args.push("show_event_date=\"no\"");
            }
            if (!_show_venue) {
                _args.push("show_event_venue=\"no\"");
            }
            if (!_show_map) {
                _args.push("show_event_map=\"no\"");
            }
            if (_args.length > 0) {
                tag = "[fb-cal " + _args.join(" ") + " /]";
            }
            if (typeof window.tinyMCE != 'undefined') {
                var tmce_ver = window.tinyMCE.majorVersion;
                if (tmce_ver >= "4") {
                    _self.execCommand('mceInsertContent', false, tag);
                } else {
                    _self.execInstanceCommand('content', 'mceInsertContent', false, tag);
                }
                _self.execCommand('mceRepaint');
                _self.close();
            }
        });
        $('#closeMCE').click(function(e) {
            e.preventDefault();
            tinyMCEPopup.close();
        })
    });
    </script>
    <style>
    .wppress-wrap {
        padding: 10px;
    }
    
    .desc {
        font-size: 80%;
        font-style: italic;
    }
    
    .checkboxes label {
        width: 50%;
        float: left;
        padding: 5px 0;
    }
    
    .checkboxes:before,
    .checkboxes:after {
        content: "";
        display: table;
        clear: both;
    }
    </style>
</head>

<body class="wp-admin wp-core-ui">
    <div id="wpwrap">
        <div class="wppress-wrap">
            <table class="form-table">
                <tr>
                    <th>
                        <label>
                            <?php _e( 'Facebook ID', 'facebook-events'); ?>
                        </label>
                    </th>
                    <td>
                        <input class="regular-text" id="page_id" type="text" placeholder="eg: 123344556677888" />
                        <div class="desc">
                            <?php _e( 'Separate with commas for multiple IDs.', 'facebook-events');?>
                        </div>
                        <div class="desc">
                            <?php _e( 'The IDs provided will be used as default IDs unless provided with individual shortcode or widget.', 'facebook-events'); ?>
                        </div>
                        <div class="desc">
                            <?php echo sprintf(__( 'You can get your profile/page/group ID from `%s` just putting the page URL.', 'facebook-events'), '<a href="http://lookup-id.com/" target="_blank">http://lookup-id.com/</a>'); ?>
                        </div>
                    </td>
                </tr>
                <?php $modes=apply_filters( 'fbcal_display_modes',array()); ?>
                <tr>
                    <th>
                        <label>
                            <?php _e( 'Display Mode', 'facebook-events'); ?>
                        </label>
                    </th>
                    <td>
                        <select id="mode">
                            <?php foreach ($modes as $mode=> $label) { ?>
                            <option value="<?php echo $mode; ?>">
                                <?php echo $label[ 'label']; ?>
                            </option>
                            <?php } ?>
                        </select>
                        <div class="desc">
                            <?php _e( 'Select display mode for the events calendar', 'facebook-events');?>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div class="checkboxes">
                            <label>
                                <input type="checkbox" id="show_event_calendar" checked="checked">
                                <?php _e( 'Show Calendar', 'facebook-events'); ?>
                            </label>
                            <label>
                                <input type="checkbox" id="show_in_popup" checked="checked">
                                <?php _e( 'Show Event Detail in Popup', 'facebook-events'); ?>
                            </label>
                            <label>
                                <input type="checkbox" id="include_attending" checked="checked">
                                <?php _e( 'Include Attending Events', 'facebook-events'); ?>
                            </label>
                            <label>
                                <input type="checkbox" id="show_event_cover" checked="checked">
                                <?php _e( 'Show Event Cover', 'facebook-events'); ?>
                            </label>
                            <label>
                                <input type="checkbox" id="show_event_date" checked="checked">
                                <?php _e( 'Show Event Date', 'facebook-events'); ?>
                            </label>
                            <label>
                                <input type="checkbox" id="show_event_venue" checked="checked">
                                <?php _e( 'Show Event Venue/Location', 'facebook-events');?>
                            </label>
                            <label>
                                <input type="checkbox" id="show_event_map" checked="checked">
                                <?php _e( 'Show Event Map Location', 'facebook-events'); ?>
                            </label>
                            <label>
                                <input type="checkbox" id="hide_past_events">
                                <?php _e( 'Hide Past Events', 'facebook-events'); ?>
                            </label>
                        </div>
                    </td>
                </tr>
            </table>
            <button id="create_shortcode" type="button" class="button button-primary">
                <?php _e( 'Insert Shortcode', 'facebook-events'); ?>
            </button>
        </div>
    </div>
</body>

</html>
