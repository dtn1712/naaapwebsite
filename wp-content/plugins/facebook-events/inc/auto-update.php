<?php
class WPPress_Update_Manager
{
    function __construct( $file, $itemid = false, $license_key = "", $license_setting_page = "",$translateDomain="wppress" )
    {
        $this->file = $file;
        $this->update_server = "https://wppress.net/";
        $this->plugin_file = $this->toUnix( $file );
        $this->item_id = $itemid;
        $this->basename = plugin_basename( $file );
        $this->slug = dirname( plugin_basename( $file ) );
        $this->license_key = $license_key;
        $this->license_setting_page = $license_setting_page;
        $this->translateDomain =$translateDomain;
        
        add_filter( 'pre_set_site_transient_update_plugins', array(
            $this,
            'check_update'
        ) );
        add_filter( 'plugins_api', array(
            $this,
            'check_info'
        ) , 10, 3 );
        add_filter( 'upgrader_pre_download', array(
            $this,
            'upgrader_pre_download'
        ) , 10, 4 );
        add_action( 'plugins_loaded', array(&$this,
            'plugins_loaded'
        ) );
    }
    function plugins_loaded()
    {
        add_action( 'in_plugin_update_message-' . $this->get_relative_path() , array(&$this,
            'add_update_message'
        ) );
    }
    function add_update_message( $plugin_data )
    {
        echo '<style type="text/css" media="all">tr#' . sanitize_title( $plugin_data['Name'] ) . ' + tr.plugin-update-tr a.thickbox + em { display: none; }</style>';
        if( $this->license_key != "" ) {
            echo '<a href="' . wp_nonce_url( admin_url( 'update.php?action=upgrade-plugin&plugin=' . $this->get_relative_path() ), 'upgrade-plugin_' . $this->get_relative_path() ) . '">' . __( 'Update plugin from WPPress.net Server', $this->translateDomain ) . '</a>';
        } 
        else {
            echo '<a href="' . $this->license_setting_page . '"><b>' . __( 'Please enter your purchase code to enable auto updates.', $this->translateDomain ) . '</b></a>';
        }
    }
    function toUnix( $path = "" )
    {
        return str_replace( "\\", "/", $path );
    }
    private function get_relative_path()
    {
        global $wp_filesystem;
        $plugin_dir = $this->toUnix( WP_PLUGIN_DIR );
        $path = str_replace( $plugin_dir, "", plugin_basename( $this->file ) );
        return $path;
    }
    
    function _plugin_get_version()
    {
        
        // populate only once
        if( !$this->plugin_version ) {
            $plugin_data = get_plugin_data( $this->plugin_file );
            $this->plugin_version = $plugin_data['Version'];
        }
        
        // return
        return $this->plugin_version;
    }
    function _get_plugin_name()
    {
        $plugin_data = get_plugin_data( $this->plugin_file );
        $this->plugin_name = $plugin_data['Name'];
        return $this->plugin_name;
    }
    function check_update( $transient )
    {
        
        if( empty( $transient->checked ) ) {
            return $transient;
        }
        
        // vars
        $info = $this->get_update_data();
        
        // validate
        if( !$info ) {
            return $transient;
        }
        
        // compare versions
        if( version_compare( $info->version, $this->_plugin_get_version() , '<=' ) ) {
            return $transient;
        }
        
        // create new object for update
        $obj = new stdClass();
        $obj->slug = $this->slug;
        $obj->new_version = $info->version;
        $obj->url = $info->homepage;
        $obj->author = $info->author;
        $obj->package = "";
         // pass this empty so we can force update from our server
        $obj->name = $info->name;
        
        // add to transient
        $transient->response[ $this->basename] = $obj;
        
        return $transient;
    }
    function get_update_data( $give_array = false )
    {
        
        // vars
        $info = false;
        $query = array();
        $query['wpp-item-id'] = $this->item_id;
        $query['wpp-item-update'] = $this->license_key != "" ? $this->license_key : "checking";
        $url = add_query_arg( $query, $this->update_server );
        
        // Get the remote info
        $request = wp_remote_get( $url );
        if( !is_wp_error( $request ) || wp_remote_retrieve_response_code( $request ) === 200 ) {
            $info = @unserialize( $request['body'] );
            if( is_object( $info ) ) {
                $info->slug = $this->slug;
            }
        }
        return $info;;
    }
    function check_info( $false, $action, $arg )
    {
        
        // validate
        if( !isset( $arg->slug ) || $arg->slug != $this->slug ) {
            return $false;
        }
        
        if( $action == 'plugin_information' ) {
            $false = $this->get_update_data();
        }
        
        return $false;
    }
    function upgrader_pre_download( $reply, $package, $updater )
    {
        global $wp_filesystem;
        if( isset( $updater->skin->plugin ) && $updater->skin->plugin === $this->get_relative_path() ) {
            $updater->strings['downloading_from_wppress'] = __( 'Downloading package from WPPress Server...', $this->translateDomain);
            $updater->skin->feedback( 'downloading_from_wppress' );
            if( $this->license_key == "" ) {
                return new WP_Error( 'no_credentials', __( 'To receive automatic updates license activation is required. Please provide Purchase Code in the settings page', $this->translateDomain ) );
            }
            
            $query = array();
            $query['wpp-item-id'] = $this->item_id;
            $query['wpp-item-download'] = $this->license_key;
            $download_url = add_query_arg( $query, $this->update_server );
            $download_file = download_url( $download_url );
            if( !is_wp_error( $download_file ) ) {
                return $download_file;
            } 
            else {
                return new WP_Error( 'download_error', __( 'There was error downloading the update. Please try again later. Or you can update the plugin by downloading from downloads page at envato marketplace', $this->translateDomain ) );
            }
        }
        
        return $reply;
    }
}
