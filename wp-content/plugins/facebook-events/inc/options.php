<style>
.form-table i.desc {
    font-size: 12px;
    font-weight: normal;
    font-style: italic;
    display: block;
}

.instructions img {
    max-width: 70%;
}
</style>
<div class="wrap">
    <div id="icon-options-general" class="icon32">
        <br>
    </div>
    <h2><?php _e('Facebook Event Calendar Settings','facebook-events'); ?></h2>
    <br/>
    <form action="options.php" method="post" id="fb-cal-form">
        <?php settings_fields( 'wppress-fb-event-calendar'); ?>
        <table class="form-table">
            <tbody>
                <tr valign="top">
                    <th scope="row">
                        <?php _e( 'Facebook ID', 'facebook-events'); ?>
                        <i class="desc"><?php _e('ID of your Facebook Profile, Page or Group','facebook-events');?></i>
                    </th>
                    <td>
                        <input name="wppress_fb_event_calendar[page_id]" type="text" value="<?php echo $this->options['page_id']; ?>" size="45" />
                        <p><i class="desc"><?php _e('Separate with commas for multiple IDs.','facebook-events');?></i>
                        </p>
                        <p><i class="desc"><?php _e('The IDs provided will be used as default IDs unless provided with individual shortcode or widget.','facebook-events'); ?></i>
                        </p>
                        <p><i class="desc"><?php echo sprintf(__('You can get your profile/page/group ID from `%s` just putting the page URL.','facebook-events'),'<a href="http://lookup-id.com/" target="_blank">http://lookup-id.com/</a>'); ?> </i></p>
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row" colspan="2">
                        <h3><?php _e('Facebook APP Setting','facebook-events'); ?></h3>
                        <i class="desc"><?php _e('You need to authenticate the calendar with your Facebook account to work.','facebook-events'); ?></i>
                    </th>
                </tr>
                <tr valign="top">
                    <th scope="row">
                        <?php _e( 'Access Token', 'facebook-events'); ?>
                        <i class="desc"><?php _e('Recommended but Optional','facebook-events'); ?></i>
                    </th>
                    <td>
                        <?php if($fb_userinfo=$this->get_fb_userinfo()){ ?>
                        <p> Authenticated By: <strong><?php echo $fb_userinfo->name; ?></strong></p>
                        <?php } ?>
                        <input name="wppress_fb_event_calendar[access_token]" type="password" id="my_access_token" value="<?php echo $this->options['access_token']; ?>" size="45" />
                        <p><i class="desc"><?php _e('You are requred to authorize the calendar APP with your Facebook account for the plugin to function properly.','facebook-events');?></i></p>
                        <p>
                            <a href="<?php echo add_query_arg(array('redirect_to'=>urlencode(admin_url('admin.php?page=fb_events_calendar' ))),$this->token_generator); ?>" class="button button-primary">
                                <?php _e( 'Authenticate with Facebook Account', 'facebook-events'); ?>
                            </a>
                            <?php if($fb_userinfo){ ?>
                            <a href="<?php echo add_query_arg(array('q'=>$this->access_token),'https://developers.facebook.com/tools/debug/accesstoken'); ?>" class="button" target="_blank">
                                <?php _e( 'Debug Access Token', 'facebook-events'); ?>
                            </a>
                            <?php } ?>
                        </p>
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row" colspan="2">
                        <h3><?php _e('Other Settings','facebook-events'); ?></h3>
                    </th>
                </tr>
                <tr valign="top">
                    <th scope="row">
                        <?php _e( 'Caching', 'facebook-events'); ?>
                        <i class="desc"><?php _e('Recommended','facebook-events'); ?></i>
                    </th>
                    <td>
                        <input name="wppress_fb_event_calendar[cache_time]" type="number" min="0" value="<?php echo $this->options['cache_time']; ?>" />
                        <select name="wppress_fb_event_calendar[cache_suffix]">
                            <?php foreach(array( "hours"=>__("Hour",'facebook-events'),"minutes"=>__("Minute",'facebook-events'),"seconds"=>__("Seconds",'facebook-events')) as $key=>$val){ ?>
                            <option value="<?php echo $key; ?>" <?php selected($key,$this->options['cache_suffix']); ?>>
                                <?php echo $val; ?>
                            </option>
                            <?php } ?>
                        </select>
                        <p><i class="desc"><?php _e('All the events feed are cached to perform best and reduce server load both on yours and facebook servers. Increasing the time would delay the events to show up until the next routine.','facebook-events'); ?> </i></p>
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">
                        <?php _e( 'Calendar Timezone', 'facebook-events'); ?>
                    </th>
                    <td>
                        <select name="wppress_fb_event_calendar[timezone]">
                            <?php foreach($this->timezones as $val=>$key){ ?>
                            <option value="<?php echo $key; ?>" <?php selected($key,$this->options['timezone']); ?>>
                                <?php echo $val; ?>
                            </option>
                            <?php } ?>
                        </select>
                        <i class="desc"><?php _e("Choose what timezone the calendar events be shown. If you want events to be shown by their local timezone, choose `<b>Visitor's Default</b>` option.",'facebook-events'); ?></i>
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">
                        <?php _e( 'First day of week', 'facebook-events'); ?>
                    </th>
                    <td>
                        <select name="wppress_fb_event_calendar[weekoffset]">
                            <?php foreach(array(0=>__("Sunday"),1=>__("Monday"),2=>__("Tuesday"),3=>__("Wednesday"),4=>__("Thursday"),5=>__("Friday"),6=>__("Saturday")) as $key=>$val){ ?>
                            <option value="<?php echo $key; ?>" <?php selected($key,$this->options['weekoffset']); ?>>
                                <?php echo $val; ?>
                            </option>
                            <?php } ?>
                        </select>
                        <i class="desc"><?php _e("Choose first day of the week.",'facebook-events'); ?></i>
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">
                        <?php _e( 'License Key', 'facebook-events'); ?>
                        <i class="desc"><?php _e('Recommended','facebook-events'); ?></i>
                    </th>
                    <td>
                        <input name="wppress_fb_event_calendar[license_key]" type="text" value="<?php echo $this->options['license_key']; ?>" size="45" />
                        <i class="desc"><?php _e('Enter the Item license key (purchase code) to enable auto-update.','facebook-events'); ?></i>
                    </td>
                </tr>
            </tbody>
        </table>
        <p class="submit">
            <input type="submit" name="submit" id="fb-cal-form-submit" class="button button-primary" value="Save Changes">
        </p>
    </form>
</div>
