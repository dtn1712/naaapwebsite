<?php
class WPPress_Facebook_Calendar_Widget extends WP_Widget
{
    function __construct()
    {
        global $wpdb;
        parent::WP_Widget( false, $name = __( 'Facebook Events Calendar', 'facebook-events' ) );
        $this->fbcalObj = FBCAL();
    }
    function widget( $args, $instance )
    {
        extract( $args );
        $ishash = false;
        $title = apply_filters( 'widget_title', $instance['title'] );
        echo $before_widget;
        if( $title ) {
            echo $before_title . $title . $after_title;
        }
        echo $this->fbcalObj->shortcode( $instance );
        echo $after_widget;
    }
    function update( $new_instance, $old_instance )
    {
        $instance = $old_instance;
        $instance['title'] = strip_tags( $new_instance['title'] );
        $instance['id'] = strip_tags( $new_instance['id'] );
        $instance['show_event_calendar'] =( $new_instance['show_event_calendar'] == "yes" ? $new_instance['show_event_calendar'] : "no" );
        $instance['include_attending'] =( $new_instance['include_attending'] == "yes" ? $new_instance['include_attending'] : "no" );
        $instance['show_in_popup'] =( $new_instance['show_in_popup'] == "yes" ? $new_instance['show_in_popup'] : "no" );
        $instance['show_event_cover'] =( $new_instance['show_event_cover'] == "yes" ? $new_instance['show_event_cover'] : "no" );
        $instance['show_event_date'] =( $new_instance['show_event_date'] == "yes" ? $new_instance['show_event_date'] : "no" );
        $instance['show_event_venue'] =( $new_instance['show_event_venue'] == "yes" ? $new_instance['show_event_venue'] : "no" );
        $instance['show_event_map'] =( $new_instance['show_event_map'] == "yes" ? $new_instance['show_event_map'] : "no" );
        $instance['hide_past_events'] =( $new_instance['hide_past_events'] == "yes" ? $new_instance['hide_past_events'] : "no" );
        $instance['mode'] = $new_instance['mode'];
        return $instance;
    }
    function form( $instance )
    {
?>

<p>
    <label for="<?php
        echo $this->get_field_id( 'title' ); ?>"> <?php
        _e( 'Title', 'facebook-events' ); ?>
        <input class="widefat" id="<?php
        echo $this->get_field_id( 'title' ); ?>" name="<?php
        echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php
        echo $instance['title']; ?>" />
    </label>
</p>
<p>
    <label for="<?php
        echo $this->get_field_id( 'id' ); ?>"> <?php
        _e( 'Facebook ID', 'facebook-events' ); ?>
        <input class="widefat" id="<?php
        echo $this->get_field_id( 'id' ); ?>" name="<?php
        echo $this->get_field_name( 'id' ); ?>" type="text" value="<?php
        echo $instance['id']; ?>" />
    </label>
     <div class="desc"><?php
        _e( 'Separate with commas for multiple IDs.', 'facebook-events' ); ?></div>
                        <div class="desc"><?php
        _e( 'The IDs provided will be used as default IDs unless provided with individual shortcode or widget.', 'facebook-events' ); ?></div>
                        <div class="desc"><?php
        echo sprintf( __( 'You can get your profile/page/group ID from `%s` just putting the page URL.', 'facebook-events' ), '<a href="http://lookup-id.com/" target="_blank">http://lookup-id.com/</a>' ); ?></div>
</p>
<?php
        $modes = apply_filters( 'fbcal_display_modes', array() ); ?>
<p>
    <label for="<?php
        echo $this->get_field_id( 'mode' ); ?>"> 
        <?php
        _e( 'Display Mode', 'facebook-events' ); ?>
    
        <select id="<?php
        echo $this->get_field_id( 'mode' ); ?>" name="<?php
        echo $this->get_field_name( 'mode' ); ?>" class="widefat">
        <?php
        foreach( $modes as $mode => $label ) { ?>
        <option value="<?php
            echo $mode; ?>" <?php
            selected( $mode, $instance['mode'] ); ?>><?php
            echo $label['label']; ?></option>
        <?php
        } ?>
        </select>
</label>         <div class="desc"><?php
        _e( 'Select display mode for the events calendar', 'facebook-events' ); ?></div>
        
    
</p>
<p>
    <label for="<?php
        echo $this->get_field_id( 'show_in_popup' ); ?>"> 
        <input class="widefat" id="<?php
        echo $this->get_field_id( 'show_in_popup' ); ?>" name="<?php
        echo $this->get_field_name( 'show_in_popup' ); ?>" type="checkbox"
        <?php
        checked( $instance['show_in_popup'], "yes" ); ?> value="yes" /><?php
        _e( 'Show Event Detail in Popup', 'facebook-events' ); ?>
    </label>
    
</p>
<p>
    <label for="<?php
        echo $this->get_field_id( 'include_attending' ); ?>"> 
        <input class="widefat" id="<?php
        echo $this->get_field_id( 'include_attending' ); ?>" name="<?php
        echo $this->get_field_name( 'include_attending' ); ?>" type="checkbox"
        <?php
        checked( $instance['include_attending'], "yes" ); ?> value="yes" /><?php
        _e( 'Include Attending Events', 'facebook-events' ); ?> <small>(<?php
        _e( 'Will require your own access token', 'facebook-events' ); ?>)</small>
    </label>
</p>

<p>
    <label for="<?php
        echo $this->get_field_id( 'show_event_cover' ); ?>"> 
        <input class="widefat" id="<?php
        echo $this->get_field_id( 'show_event_cover' ); ?>" name="<?php
        echo $this->get_field_name( 'show_event_cover' ); ?>" type="checkbox"
        <?php
        checked( $instance['show_event_cover'], "yes" ); ?> value="yes" /><?php
        _e( 'Show Event Cover', 'facebook-events' ); ?>
    </label>
</p>
<p>
    <label for="<?php
        echo $this->get_field_id( 'show_event_calendar' ); ?>"> 
        <input class="widefat" id="<?php
        echo $this->get_field_id( 'show_event_calendar' ); ?>" name="<?php
        echo $this->get_field_name( 'show_event_calendar' ); ?>" type="checkbox"
        <?php
        checked( $instance['show_event_calendar'], "yes" ); ?> value="yes" /><?php
        _e( 'Show Events Calendar', 'facebook-events' ); ?>
    </label>
</p>
<p>
    <label for="<?php
        echo $this->get_field_id( 'show_event_date' ); ?>"> 
        <input class="widefat" id="<?php
        echo $this->get_field_id( 'show_event_date' ); ?>" name="<?php
        echo $this->get_field_name( 'show_event_date' ); ?>" type="checkbox"
        <?php
        checked( $instance['show_event_date'], "yes" ); ?> value="yes" /><?php
        _e( 'Show Event Date', 'facebook-events' ); ?>
    </label>
</p>
<p>
    <label for="<?php
        echo $this->get_field_id( 'show_event_venue' ); ?>"> 
        <input class="widefat" id="<?php
        echo $this->get_field_id( 'show_event_venue' ); ?>" name="<?php
        echo $this->get_field_name( 'show_event_venue' ); ?>" type="checkbox"
        <?php
        checked( $instance['show_event_venue'], "yes" ); ?> value="yes" /><?php
        _e( 'Show Event Venue/Location', 'facebook-events' ); ?>
    </label>
    
</p>
<p>
    <label for="<?php
        echo $this->get_field_id( 'show_event_map' ); ?>"> 
        <input class="widefat" id="<?php
        echo $this->get_field_id( 'show_event_map' ); ?>" name="<?php
        echo $this->get_field_name( 'show_event_map' ); ?>" type="checkbox"
        <?php
        checked( $instance['show_event_map'], "yes" ); ?> value="yes" /><?php
        _e( 'Show Event Map', 'facebook-events' ); ?>
    </label>
</p>
<p>
    <label for="<?php
        echo $this->get_field_id( 'hide_past_events' ); ?>"> 
        <input class="widefat" id="<?php
        echo $this->get_field_id( 'hide_past_events' ); ?>" name="<?php
        echo $this->get_field_name( 'hide_past_events' ); ?>" type="checkbox"
        <?php
        checked( $instance['hide_past_events'], "yes" ); ?> value="yes" /><?php
        _e( 'Hide Past Events', 'facebook-events' ); ?>
    </label>
    
</p>


<?php
    }
}
add_action( 'widgets_init', create_function( '', 'return register_widget("WPPress_Facebook_Calendar_Widget");' ) );
