<?php
/**
 * Single Product Image
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-image.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.14
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post, $woocommerce, $product, $shop_single_image, $shop_thumbnail_image;
$attachment_ids = $product->get_gallery_attachment_ids();
$attachment_ids = array_merge( array( get_post_thumbnail_id() ), $attachment_ids );
$image_width = isset( $shop_single_image['width'] ) ? $shop_single_image['width'] : '480';
?>
<div class="product_image_wrap"  style="width:<?php echo $image_width + 20; ?>px">

    <div class="images flexslider" id="product_image">

		<?php
		if ( $attachment_ids ) {
			wp_enqueue_script( 'flexslider' );
			?>
            <ul class="slides">
				<?php
				foreach ( $attachment_ids as $attachment_id ) {
					$image_title = esc_attr( get_the_title( $attachment_id ) );
					$image_caption = get_post( $attachment_id )->post_excerpt;
					$image_link = wp_get_attachment_url( $attachment_id );

					if ( ! $image_link ) {
						continue; }
					ob_start();
					theme_post_thumbnail_src( $attachment_id, apply_filters( 'single_product_large_thumbnail_size', 'churchope_shop_single' ) );
					$image = ob_get_clean();

					echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '<li><a href="%s" itemprop="image"  class="woocommerce-main-image zoom" title="%s" data-rel="prettyPhoto[product-images]">%s</a></li>', $image_link, $image_title, $image ), $attachment_id, $post->ID );
				}
				?>
            </ul>
			<?php
		} else {

			echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '<img src="%s" alt="%s" />', wc_placeholder_img_src(), __( 'Placeholder', 'woocommerce' ) ), $post->ID );
		}
		?>



    </div>
	<?php
	/**
	 * woocommerce_after_single_product_main_images hook
	 *
	 * @hooked woocommerce_show_product_sale_flash - 10
	 */
	do_action( 'woocommerce_after_single_product_main_images' );
	?>
	<?php do_action( 'woocommerce_product_thumbnails' ); ?>
</div>
