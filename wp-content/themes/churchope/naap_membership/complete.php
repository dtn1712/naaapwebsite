<?php
/*==================================================================
 PayPal Express Checkout Call
 ===================================================================
*/
// Check to see if the Request object contains a variable named 'token'

ob_start();

require_once ("../../../../wp-config.php");
require_once ('Mandrill.php');
get_header();

function sendEmail(){

	$mandrillVal = get_option("wpmandrill");

	try {
	    $mandrill = new Mandrill($mandrillVal['api_key']);
	    $message = array(
	        'text' => 
"Thank you for supporting NAAAP-Seattle's 37th Annual Gala. Your ticket purchase has been confirmed.

Date: Saturday, November 5th, 2016

Location: Motif Seattle, 1415 5th Ave, Seattle, WA 98101
						
Dress Code: Black Tie Preferred
						
Program: 

5:00pm / Registration, Social & Silent Auction
						
6:00pm / Dinner and Program
						
Additional details will be sent to the email address you provided. For questions about the gala, meal request or dietary restrictions, please contact stephanie.lau@naaapseattle.org. 

We look forward to seeing you at this year's celebration!  
						
*************************************************************************************************************
Transaction Receipt: 
Please print the following for your records.

NAAAP-Seattle's 37th Annual Gala

Saturday, November 5th, 2016

Description\t\tPrice\tQuantity\tTotal
General Admission\t$".$_SESSION['Payment_Amount'] / $_SESSION['number']."\t\t".$_SESSION['number']."\t\t$".$_SESSION['Payment_Amount']."

Discount Code Applied: ". $_SESSION['discountcode'] ."
Your Information:

First Name: ".$_SESSION['firstname']."

Last Name: ". $_SESSION['lastname']."

Company: ". $_SESSION['company']."

Email: ". $_SESSION['email'],

	        'subject' => 'Thank you for your NAAAP-Seattle Gala ticket purchase.',
	        'from_email' => $mandrillVal['from_username'],
	        'from_name' => $mandrillVal['from_name'],
	        'to' => array(
	            array(
	                'email' => $_SESSION['email'],
	                'name' => $_SESSION['firstname']." ".$_SESSION['lastname'],
	                'type' => 'to'
	            )
	        ),
	        'important' => false,
	        'track_opens' => null,
	        'track_clicks' => null,
	        'auto_text' => null,
	        'auto_html' => null,
	        'inline_css' => null,
	        'url_strip_qs' => null,
	        'preserve_recipients' => null,
	        'view_content_link' => null,
	        'tracking_domain' => null,
	        'signing_domain' => null,
	        'return_path_domain' => null,
	        'merge' => true,
	        'merge_language' => 'mailchimp',
	    );
	    $async = false;
	    $result = $mandrill->messages->send($message, $async);
	} catch(Mandrill_Error $e) {
	    echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
	}

}

function handleCompletePayment() {
	global $wpdb,$table_prefix;
	$wpdb->show_errors = true;
	$wpdb->suppress_errors = false;
	$username = $_SESSION['iduser'];
	$fname = $_SESSION['firstname'];
	$lname = $_SESSION['lastname'];
	$email = $_SESSION['email'];
	$number = $_SESSION['number'];
	$totalAmount = $_SESSION['Payment_Amount'];
	$company = $_SESSION['company'];
	$discountCode = $_SESSION['discountcode'];
  	$wpdb->insert($table_prefix.'memberships_users',
                              array(
                                    'iduser'    => $username,
                                    'firstname' => $fname,
                                    'lastname'  => $lname,
                                    'email'     => $email,
                                    'number'    => $number,
                                    'totalamount' => $totalAmount,
                                    'company'	=> $company,
                                    "discountcode" => $discountCode
                              ));
  	if($wpdb->insert_id == false) {
  		wp_die("Fail!");
	} else {

		sendEmail();

		unset($_SESSION['Payment_Amount']);
		unset($_SESSION['Payment_Option']);
		unset($_SESSION['iduser']);
		unset($_SESSION['firstname']);
		unset($_SESSION['lastname']);
		unset($_SESSION['email']);
		unset($_SESSION['number']);
		unset($_SESSION['company']);
		unset($_SESSION['discountcode']);
		
		$_SESSION['buyticket'] = 'success';

		header('Location:'.$_SESSION['ticketlink']);
	}
}

error_reporting(0);
$token = "";
if (isset($_REQUEST['token']))
{
	$token = $_REQUEST['token'];

}

// If the Request object contains the variable 'token' then it means that the user is coming from PayPal site.
if ( $token != "" )
{

	require_once ("paypalfunctions.php");

	/*
	'------------------------------------
	' Calls the GetExpressCheckoutDetails API call
	'
	' The GetShippingDetails function is defined in PayPalFunctions.jsp
	' included at the top of this file.
	'-------------------------------------------------
	*/


	$resArray = GetShippingDetails( $token );
	$ack = strtoupper($resArray["ACK"]);
	if( $ack == "SUCCESS" || $ack == "SUCESSWITHWARNING")
	{
		/*
		' The information that is returned by the GetExpressCheckoutDetails call should be integrated by the partner into his Order Review
		' page
		*/
		$email 				= $resArray["EMAIL"]; // ' Email address of payer.
		$payerId 			= $resArray["PAYERID"]; // ' Unique PayPal customer account identification number.
		$payerStatus		= $resArray["PAYERSTATUS"]; // ' Status of payer. Character length and limitations: 10 single-byte alphabetic characters.
		$salutation			= $resArray["SALUTATION"]; // ' Payer's salutation.
		$firstName			= $resArray["FIRSTNAME"]; // ' Payer's first name.
		$middleName			= $resArray["MIDDLENAME"]; // ' Payer's middle name.
		$lastName			= $resArray["LASTNAME"]; // ' Payer's last name.
		$suffix				= $resArray["SUFFIX"]; // ' Payer's suffix.
		$cntryCode			= $resArray["COUNTRYCODE"]; // ' Payer's country of residence in the form of ISO standard 3166 two-character country codes.
		$business			= $resArray["BUSINESS"]; // ' Payer's business name.
		$shipToName			= $resArray["PAYMENTREQUEST_0_SHIPTONAME"]; // ' Person's name associated with this address.
		$shipToStreet		= $resArray["PAYMENTREQUEST_0_SHIPTOSTREET"]; // ' First street address.
		$shipToStreet2		= $resArray["PAYMENTREQUEST_0_SHIPTOSTREET2"]; // ' Second street address.
		$shipToCity			= $resArray["PAYMENTREQUEST_0_SHIPTOCITY"]; // ' Name of city.
		$shipToState		= $resArray["PAYMENTREQUEST_0_SHIPTOSTATE"]; // ' State or province
		$shipToCntryCode	= $resArray["PAYMENTREQUEST_0_SHIPTOCOUNTRYCODE"]; // ' Country code.
		$shipToZip			= $resArray["PAYMENTREQUEST_0_SHIPTOZIP"]; // ' U.S. Zip code or other country-specific postal code.
		$addressStatus 		= $resArray["ADDRESSSTATUS"]; // ' Status of street address on file with PayPal
		$invoiceNumber		= $resArray["INVNUM"]; // ' Your own invoice or tracking number, as set by you in the element of the same name in SetExpressCheckout request .
		$phonNumber			= $resArray["PHONENUM"]; // ' Payer's contact telephone number. Note:  PayPal returns a contact telephone number only if your Merchant account profile settings require that the buyer enter one.
	}
	else
	{
		//Display a user friendly Error on the page using any of the following error information returned by PayPal
		$ErrorCode = urldecode($resArray["L_ERRORCODE0"]);
		$ErrorShortMsg = urldecode($resArray["L_SHORTMESSAGE0"]);
		$ErrorLongMsg = urldecode($resArray["L_LONGMESSAGE0"]);
		$ErrorSeverityCode = urldecode($resArray["L_SEVERITYCODE0"]);

		echo "GetExpressCheckoutDetails API call failed. ";
		echo "Detailed Error Message: " . $ErrorLongMsg;
		echo "Short Error Message: " . $ErrorShortMsg;
		echo "Error Code: " . $ErrorCode;
		echo "Error Severity Code: " . $ErrorSeverityCode;
	}


	/*
	'------------------------------------
	' The paymentAmount is the total value of
	' the shopping cart, that was set
	' earlier in a session variable
	' by the shopping cart page
	'------------------------------------
	*/

	$finalPaymentAmount =  $_SESSION["Payment_Amount"];

	/*
	'------------------------------------
	' Calls the DoExpressCheckoutPayment API call
	'
	' The ConfirmPayment function is defined in the file PayPalFunctions.jsp,
	' that is included at the top of this file.
	'-------------------------------------------------
	*/

	$resArray = ConfirmPayment ( $finalPaymentAmount );
	$ack = strtoupper($resArray["ACK"]);
	if( $ack == "SUCCESS" || $ack == "SUCCESSWITHWARNING" )
	{
		/*
		'********************************************************************************************************************
		'
		' THE PARTNER SHOULD SAVE THE KEY TRANSACTION RELATED INFORMATION LIKE
		'                    transactionId & orderTime
		'  IN THEIR OWN  DATABASE
		' AND THE REST OF THE INFORMATION CAN BE USED TO UNDERSTAND THE STATUS OF THE PAYMENT
		'
		'********************************************************************************************************************
		*/

		$transactionId		= $resArray["PAYMENTINFO_0_TRANSACTIONID"]; // ' Unique transaction ID of the payment. Note:  If the PaymentAction of the request was Authorization or Order, this value is your AuthorizationID for use with the Authorization & Capture APIs.
		$transactionType 	= $resArray["PAYMENTINFO_0_TRANSACTIONTYPE"]; //' The type of transaction Possible values: l  cart l  express-checkout
		$paymentType		= $resArray["PAYMENTINFO_0_PAYMENTTYPE"];  //' Indicates whether the payment is instant or delayed. Possible values: l  none l  echeck l  instant
		$orderTime 			= $resArray["PAYMENTINFO_0_ORDERTIME"];  //' Time/date stamp of payment
		$amt				= $resArray["PAYMENTINFO_0_AMT"];  //' The final amount charged, including any shipping and taxes from your Merchant Profile.
		$currencyCode		= $resArray["PAYMENTINFO_0_CURRENCYCODE"];  //' A three-character currency code for one of the currencies listed in PayPay-Supported Transactional Currencies. Default: USD.
		$feeAmt				= $resArray["PAYMENTINFO_0_FEEAMT"];  //' PayPal fee amount charged for the transaction
		$settleAmt			= $resArray["PAYMENTINFO_0_SETTLEAMT"];  //' Amount deposited in your PayPal account after a currency conversion.
		$taxAmt				= $resArray["PAYMENTINFO_0_TAXAMT"];  //' Tax charged on the transaction.
		$exchangeRate		= $resArray["PAYMENTINFO_0_EXCHANGERATE"];  //' Exchange rate if a currency conversion occurred. Relevant only if your are billing in their non-primary currency. If the customer chooses to pay with a currency other than the non-primary currency, the conversion occurs in the customer's account.

		/*
		' Status of the payment:
				'Completed: The payment has been completed, and the funds have been added successfully to your account balance.
				'Pending: The payment is pending. See the PendingReason element for more information.
		*/

		$paymentStatus	= $resArray["PAYMENTINFO_0_PAYMENTSTATUS"];

		/*
		'The reason the payment is pending:
		'  none: No pending reason
		'  address: The payment is pending because your customer did not include a confirmed shipping address and your Payment Receiving Preferences is set such that you want to manually accept or deny each of these payments. To change your preference, go to the Preferences section of your Profile.
		'  echeck: The payment is pending because it was made by an eCheck that has not yet cleared.
		'  intl: The payment is pending because you hold a non-U.S. account and do not have a withdrawal mechanism. You must manually accept or deny this payment from your Account Overview.
		'  multi-currency: You do not have a balance in the currency sent, and you do not have your Payment Receiving Preferences set to automatically convert and accept this payment. You must manually accept or deny this payment.
		'  verify: The payment is pending because you are not yet verified. You must verify your account before you can accept this payment.
		'  other: The payment is pending for a reason other than those listed above. For more information, contact PayPal customer service.
		*/

		$pendingReason	= $resArray["PAYMENTINFO_0_PENDINGREASON"];

		/*
		'The reason for a reversal if TransactionType is reversal:
		'  none: No reason code
		'  chargeback: A reversal has occurred on this transaction due to a chargeback by your customer.
		'  guarantee: A reversal has occurred on this transaction due to your customer triggering a money-back guarantee.
		'  buyer-complaint: A reversal has occurred on this transaction due to a complaint about the transaction from your customer.
		'  refund: A reversal has occurred on this transaction because you have given the customer a refund.
		'  other: A reversal has occurred on this transaction due to a reason not listed above.
		*/

		$reasonCode		= $resArray["PAYMENTINFO_0_REASONCODE"];
	}
	else
	{
		//Display a user friendly Error on the page using any of the following error information returned by PayPal
		$ErrorCode = urldecode($resArray["L_ERRORCODE0"]);
		$ErrorShortMsg = urldecode($resArray["L_SHORTMESSAGE0"]);
		$ErrorLongMsg = urldecode($resArray["L_LONGMESSAGE0"]);
		$ErrorSeverityCode = urldecode($resArray["L_SEVERITYCODE0"]);

		echo "GetExpressCheckoutDetails API call failed. ";
		echo "Detailed Error Message: " . $ErrorLongMsg;
		echo "Short Error Message: " . $ErrorShortMsg;
		echo "Error Code: " . $ErrorCode;
		echo "Error Severity Code: " . $ErrorSeverityCode;
	}

	if ($resArray["PAYMENTINFO_0_ACK"]=="Success"){
	  	handleCompletePayment();
	} else {
		$_SESSION['buyticket'] = 'failed';
		header('Location:'.$_SESSION['ticketlink']);
	}
} else if ($_SESSION['Payment_Amount'] == 0) {
	handleCompletePayment();
} else {
	header('Location:/');
}

?>

<?php get_footer(); ?>
