<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<?php
		if ( ! get_option( SHORTNAME . '_gfontdisable' ) ) {
		?>
			<link href='//fonts.googleapis.com/css?family=<?php
			$gfont = array();
			if ( get_option( SHORTNAME . '_preview' ) != '' ) {
				if ( isset( $_SESSION[ SHORTNAME . '_gfont' ] ) ) {
					$gfont[] = stripslashes( $_SESSION[ SHORTNAME . '_gfont' ] );
				}
				if ( isset( $_SESSION[ SHORTNAME . '_gfont_menu' ] ) ) {
					$gfont[] = stripslashes( $_SESSION[ SHORTNAME . '_gfont_menu' ] );
				}
			} else {

				$gfont[] = get_option( SHORTNAME . '_gfont' );
				$gfont[] = get_option( SHORTNAME . '_gfont_menu' );
			}

			if ( ! count( $gfont ) ) {
				// default style
				$gfont[] = '[{"family":"Open Sans","variants":["300italic","400italic","600italic","700italic","800italic","400","300","600","700","800"],"subsets":[]}]';
			}

			echo Admin_Theme_Element_Select_Gfont::font_queue( $gfont );
			?>' rel='stylesheet' type='text/css'>
				<?php } ?>
		<meta charset="<?php bloginfo( 'charset' ); ?>">

        <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="author" content="<?php echo home_url(); ?>">    
		<link rel="alternate" type="application/rss+xml" title="<?php bloginfo( 'name' ); ?> Feed" href="<?php bloginfo( 'rss2_url' ); ?>">         

		<?php
		global $post, $post_layout;

		// Post id
		if ( ! is_404() && ! is_search() ) {
			$pid = get_the_ID();
		} else {
			$pid = null;
		}
		if ( is_home() && get_option( 'page_for_posts' ) ) {
			$pid = get_option( 'page_for_posts' );
		}

		// Post layout
		if ( is_single() && $post->post_type == 'post' ) {
			$post_layout = (get_post_meta( $pid, SHORTNAME . '_post_layout', true )) ? get_post_meta( $pid, SHORTNAME . '_post_layout', true ) : 'layout_' . get_option( SHORTNAME . '_post_layout' ) . '_sidebar';
		} elseif ( is_single() && $post->post_type == Custom_Posts_Type_Event::POST_TYPE ) {
			$post_layout = (get_post_meta( $pid, SHORTNAME . '_post_layout', true )) ? get_post_meta( $pid, SHORTNAME . '_post_layout', true ) : 'layout_' . get_option( SHORTNAME . '_events_layout' ) . '_sidebar';
		} elseif ( is_single() && $post->post_type == Custom_Posts_Type_Sermon::POST_TYPE ) {
			$post_layout = (get_post_meta( $pid, SHORTNAME . '_post_layout', true )) ? get_post_meta( $pid, SHORTNAME . '_post_layout', true ) : 'layout_' . get_option( SHORTNAME . '_sermons_layout' ) . '_sidebar';
		} elseif ( is_single() && $post->post_type == Custom_Posts_Type_Gallery::POST_TYPE ) {
			$post_layout = (get_post_meta( $pid, SHORTNAME . '_post_layout', true )) ? get_post_meta( $pid, SHORTNAME . '_post_layout', true ) : 'layout_' . get_option( SHORTNAME . '_gallery_layout' ) . '_sidebar';
		} elseif ( is_single() && $post->post_type == Custom_Posts_Type_Testimonial::POST_TYPE ) {
			$post_layout = (get_post_meta( $pid, SHORTNAME . '_post_layout', true )) ? get_post_meta( $pid, SHORTNAME . '_post_layout', true ) : 'layout_' . get_option( SHORTNAME . '_testimonial_layout' ) . '_sidebar';
		} elseif ( is_single() && $post->post_type == 'product' ) {
			$post_layout = (get_post_meta( $pid, SHORTNAME . '_post_layout', true )) ? get_post_meta( $pid, SHORTNAME . '_post_layout', true ) : 'layout_' . get_option( SHORTNAME . '_product_layout' ) . '_sidebar';
		} elseif ( is_category() || is_tag() ) {
			global $wp_query;
			$term = $wp_query->get_queried_object();
			$post_layout = (get_tax_meta( $term->term_id, SHORTNAME . '_post_listing_layout', true )) ? get_tax_meta( $term->term_id, SHORTNAME . '_post_listing_layout', true ) : 'layout_' . get_option( SHORTNAME . '_post_listing_layout' ) . '_sidebar';
		} elseif ( is_tax( Custom_Posts_Type_Gallery::TAXONOMY ) ) {
			global $wp_query;
			$term = $wp_query->get_queried_object();
			$post_layout = (get_tax_meta( $term->term_id, SHORTNAME . '_post_listing_layout', true )) ? get_tax_meta( $term->term_id, SHORTNAME . '_post_listing_layout', true ) : 'layout_' . get_option( SHORTNAME . '_galleries_listing_layout' ) . '_sidebar';
		} elseif ( is_tax( Custom_Posts_Type_Testimonial::TAXONOMY ) ) {
			global $wp_query;
			$term = $wp_query->get_queried_object();
			$post_layout = (get_tax_meta( $term->term_id, SHORTNAME . '_post_listing_layout', true )) ? get_tax_meta( $term->term_id, SHORTNAME . '_post_listing_layout', true ) : 'layout_' . get_option( SHORTNAME . '_testimonials_listing_layout' ) . '_sidebar';
		} elseif ( is_tax( Custom_Posts_Type_Sermon::TAXONOMY ) ) {
			global $wp_query;
			$term = $wp_query->get_queried_object();
			$post_layout = (get_tax_meta( $term->term_id, SHORTNAME . '_post_listing_layout', true )) ? get_tax_meta( $term->term_id, SHORTNAME . '_post_listing_layout', true ) : 'layout_' . get_option( SHORTNAME . '_sermons_listing_layout' ) . '_sidebar';
		} elseif ( is_tax( Custom_Posts_Type_Event::TAXONOMY ) ) {
			global $wp_query;
			$term = $wp_query->get_queried_object();
			$post_layout = (get_tax_meta( $term->term_id, SHORTNAME . '_post_listing_layout', true )) ? get_tax_meta( $term->term_id, SHORTNAME . '_post_listing_layout', true ) : 'layout_' . get_option( SHORTNAME . '_events_listing_layout' ) . '_sidebar';
		} elseif ( is_tax( 'product_cat' ) || is_tax( 'product_tag' ) ) {
			global $wp_query;
			$term = $wp_query->get_queried_object();
			if ( get_tax_meta( $term->term_id, SHORTNAME . '_post_listing_layout', true ) ) {
				$post_layout = get_tax_meta( $term->term_id, SHORTNAME . '_post_listing_layout', true );
			} else {
				$pid = wc_get_page_id( 'shop' );
				if ( get_option( SHORTNAME . '_products_listing_layout' ) ) {
					$post_layout = 'layout_' . get_option( SHORTNAME . '_products_listing_layout' ) . '_sidebar';
				} elseif ( get_post_meta( $pid, '_wp_page_template', true ) == 'template-leftsidebar.php' ) {
					$post_layout = 'layout_left_sidebar';
				} elseif ( get_post_meta( $pid, '_wp_page_template', true ) == 'template-rightsidebar.php' ) {
					$post_layout = 'layout_right_sidebar';
				} else {
					$post_layout = 'layout_none_sidebar';
				}
			}
		} elseif ( is_post_type_archive( 'product' ) ) {
			if ( is_shop() ) {
				$pid = wc_get_page_id( 'shop' );
				if ( get_option( SHORTNAME . '_products_listing_layout' ) ) {
					$post_layout = 'layout_' . get_option( SHORTNAME . '_products_listing_layout' ) . '_sidebar';
				} elseif ( get_post_meta( $pid, '_wp_page_template', true ) == 'template-leftsidebar.php' ) {
					$post_layout = 'layout_left_sidebar';
				} elseif ( get_post_meta( $pid, '_wp_page_template', true ) == 'template-rightsidebar.php' ) {
					$post_layout = 'layout_right_sidebar';
				} else {
					$post_layout = 'layout_none_sidebar';
				}
			} else {
				if ( get_post_meta( $pid, '_wp_page_template', true ) == 'template-leftsidebar.php' ) {
					$post_layout = 'layout_left_sidebar';
				} elseif ( get_post_meta( $pid, '_wp_page_template', true ) == 'template-rightsidebar.php' ) {
					$post_layout = 'layout_right_sidebar';
				} else {
					$post_layout = 'layout_none_sidebar';
				}
			}
		} elseif ( is_home() || (is_404() && get_query_var( 'pagename' ) != 'customeventslist') || is_search() || is_date() ) {
			$post_layout = 'layout_' . get_option( SHORTNAME . '_post_listing_layout' ) . '_sidebar';
		} elseif ( is_page() ) {
			if ( get_post_meta( $pid, '_wp_page_template', true ) == 'template-leftsidebar.php' ) {
				$post_layout = 'layout_left_sidebar';
			} elseif ( get_post_meta( $pid, '_wp_page_template', true ) == 'template-rightsidebar.php' ) {
				$post_layout = 'layout_right_sidebar';
			} else {
				$post_layout = 'layout_none_sidebar';
			}
		} elseif ( get_query_var( 'pagename' ) == 'customeventslist' ) {
			$post_layout = 'layout_' . get_option( SHORTNAME . '_events_listing_layout' ) . '_sidebar';
		} else {
			$post_layout = 'layout_none_sidebar';
		}

		/**
		 * slideshow...
		 */
		global $wp_query;
		$current_term = $wp_query->get_queried_object();


		if ( is_post_type_archive( 'product' ) ) {
			if ( is_shop() ) {
				$pid = wc_get_page_id( 'shop' );
				if ( get_post_meta( $pid, '_post_slider', true ) !== 'Disable' ) {
					$slideshow = 'th_slideshow';
				} else {
					$slideshow = '';
				}
			} else {
				if ( get_post_meta( $pid, '_post_slider', true ) !== 'Disable' ) {
					$slideshow = 'th_slideshow';
				} else {
					$slideshow = '';
				}
			}
		} elseif ( (is_tax() || is_tag() || is_category()) && $current_term && get_tax_meta( $current_term->term_id, SHORTNAME . '_tax_slider', true ) && (get_tax_meta( $current_term->term_id, SHORTNAME . '_tax_slider', true ) !== 'Disable') ) {
			$slideshow = 'th_slideshow';
		} //post page
		elseif ( ! is_archive() && get_post_meta( $pid, SHORTNAME . '_post_slider', true ) && (get_post_meta( $pid, SHORTNAME . '_post_slider', true ) !== 'Disable') ) {
			$slideshow = 'th_slideshow';
		} elseif ( is_home() && get_post_meta( get_option( 'page_for_posts' ), SHORTNAME . '_post_slider', true ) && get_option( 'page_on_front' ) ) {
			$slideshow = 'th_slideshow';
		} // Reading settings Blog page undefined and Front page undefined and Blog listing slider disabled
		elseif ( is_home() && ! get_post_meta( get_option( 'page_for_posts' ), SHORTNAME . '_post_slider', true ) && ! get_option( 'page_on_front' ) && get_option( SHORTNAME . '_blog_slider' ) && get_option( SHORTNAME . '_blog_slider' ) !== 'Disable' && get_option( SHORTNAME . '_blog_slider' ) !== 'Use global' ) {
			$slideshow = 'th_slideshow';
		} elseif ( is_home() && ! get_post_meta( get_option( 'page_for_posts' ), SHORTNAME . '_post_slider', true ) && ! get_option( 'page_on_front' ) && get_option( SHORTNAME . '_blog_slider' ) && get_option( SHORTNAME . '_blog_slider' ) == 'Disable' ) {
			$slideshow = '';
		} elseif ( $current_term && (isset( $current_term->term_id ) && get_tax_meta( $current_term->term_id, SHORTNAME . '_tax_slider', true ) == 'Disable') || ($pid && get_post_meta( $pid, SHORTNAME . '_post_slider', true ) == 'Disable') ) {
			$slideshow = '';
		} //global slideshow settings
		else {
			$slideshow = (get_option( SHORTNAME . '_global_slider' ) !== 'Disable') ? 'th_slideshow' : '';
		}

		$widget_title = (get_post_meta( $pid, SHORTNAME . '_title_sidebar', true )) ? 'widget_title' : '';
		$boxed = (get_option( SHORTNAME . '_preview' ) && isset( $_SESSION[ SHORTNAME . '_boxed' ] ) && $_SESSION[ SHORTNAME . '_boxed' ] === '1') ? 'boxed' : (get_option( SHORTNAME . '_boxed' )) ? 'boxed' : '';


		// custom header background
		$custom_header_settings = false;
		if ( is_post_type_archive( 'product' ) ) {
			$pid = wc_get_page_id( 'shop' );
			if ( is_shop() && get_post_meta( $pid, SHORTNAME . '_custom_post_header', true ) ) {

				$custom_header_settings = true;
				$custom_header_color = get_post_meta( $pid, SHORTNAME . '_page_header_color', true );
				$custom_header_pattern = get_post_meta( $pid, SHORTNAME . '_post_menupattern', true );
				$custom_header_repeat = get_post_meta( $pid, SHORTNAME . '_post_headerpattern_repeat', true );
				$custom_header_horizontal = get_post_meta( $pid, SHORTNAME . '_post_headerpattern_x', true );
				$custom_header_vertical = get_post_meta( $pid, SHORTNAME . '_post_headerpattern_y', true );
			}
		} elseif ( is_singular() && get_post_meta( $pid, SHORTNAME . '_custom_post_header', true ) ) {
			$custom_header_settings = true;
			$custom_header_color = get_post_meta( $pid, SHORTNAME . '_page_header_color', true );
			$custom_header_pattern = get_post_meta( $pid, SHORTNAME . '_post_menupattern', true );
			$custom_header_repeat = get_post_meta( $pid, SHORTNAME . '_post_headerpattern_repeat', true );
			$custom_header_horizontal = get_post_meta( $pid, SHORTNAME . '_post_headerpattern_x', true );
			$custom_header_vertical = get_post_meta( $pid, SHORTNAME . '_post_headerpattern_y', true );
		} elseif ( isset( $current_term->term_id ) && get_tax_meta( $current_term->term_id, SHORTNAME . '_custom_term_header', true ) ) {
			$custom_header_settings = true;
			$term_id = $current_term->term_id;

			$custom_header_color = get_tax_meta( $term_id, SHORTNAME . '_term_header_color', true );
			$custom_header_pattern = get_tax_meta( $term_id, SHORTNAME . '_term_menupattern', true );
			$custom_header_repeat = get_tax_meta( $term_id, SHORTNAME . '_term_headerpattern_repeat', true );
			$custom_header_horizontal = get_tax_meta( $term_id, SHORTNAME . '_term_headerpattern_x', true );
			$custom_header_vertical = get_tax_meta( $term_id, SHORTNAME . '_term_headerpattern_y', true );
		} elseif ( is_home() && get_option( 'page_for_posts' ) ) {

			$pid = get_option( 'page_for_posts' );
			if ( get_post_meta( $pid, SHORTNAME . '_custom_post_header', true ) ) {

				$custom_header_settings = true;
				$custom_header_color = get_post_meta( $pid, SHORTNAME . '_page_header_color', true );
				$custom_header_pattern = get_post_meta( $pid, SHORTNAME . '_post_menupattern', true );
				$custom_header_repeat = get_post_meta( $pid, SHORTNAME . '_post_headerpattern_repeat', true );
				$custom_header_horizontal = get_post_meta( $pid, SHORTNAME . '_post_headerpattern_x', true );
				$custom_header_vertical = get_post_meta( $pid, SHORTNAME . '_post_headerpattern_y', true );
			}
		}

		if ( ! empty( $custom_header_settings ) ) {
			echo "<style>
                    #color_header {background:$custom_header_color url('$custom_header_pattern') $custom_header_repeat $custom_header_horizontal $custom_header_vertical !important; }
				</style>";
		}

		 wp_head();
			?>     
    </head>
	<body <?php body_class( $post_layout . ' ' . $slideshow . ' ' . $widget_title . ' ' . $boxed ); ?>>
		<?php echo ($boxed) ? '<div class="wrapper">' : '' ?>
		  <!--[if lt IE 7]><?php _e( '<p class=chromeframe>Your browser is <em>ancient!</em> <a href="http://browsehappy.com/">Upgrade to a different browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to experience this site.</p>', 'churchope' ); ?><![endif]-->
		<?php if ( get_option( SHORTNAME . '_show_top_line' ) ) :  ?>
            <div class="topline">
                <div class="row clearfix">
					<div class="topline-content <?php echo get_top_line_grid(); ?>">
						<?php
						if ( $header_tinymce_content = get_option( SHORTNAME . '_topline_tinymce' ) ) {
							echo th_the_content( $header_tinymce_content );
						}
						?>
                    </div>
					<?php if ( th_woocommerce_activated() && get_option( SHORTNAME . '_topline_login' ) ) :  ?>
                        <div class="topline-login grid_4">
							<?php th_shop_login(); ?>
                        </div>
					<?php endif; ?>
                </div>
            </div>
		<?php endif; // topline      ?>

        <header class="clearfix header">
            <div class="header_bottom">
                <div class="header_top clearfix">
                    <div class="row">
						<?php if ( th_woocommerce_activated() && get_option( SHORTNAME . '_topline_cart' ) ) :  ?>
							<?php global $woocommerce; ?>
                            <div class="topline-cart">
                                <span class="ribbon_bg"></span>
								<span class="top_cart_text ribbon"><span><?php echo sprintf( '%1$d', $woocommerce->cart->cart_contents_count ); ?></span></span>

                                <div class="topline_shopping_cart widget_shopping_cart" style="display:none; z-index: 100002; opacity: 0;">
									<?php _e( '<h3 class="topline-heading">Shopping cart</h3>', 'churchope' ); ?>
                                    <div class="widget_shopping_cart_content">
										<?php woocommerce_mini_cart(); ?>
                                    </div>
                                </div>
                            </div>
						<?php endif; ?>
						<div class="logo grid grid_5<?php
						if ( th_woocommerce_activated() && get_option( SHORTNAME . '_topline_cart' ) ) {
							echo (' topline-cart-logo-indent');
						}
						?>">
									<?php
									if ( get_option( SHORTNAME . '_logo_txt' ) || get_option( SHORTNAME . '_logo_custom' ) ) {
									?>
										<?php
										if ( is_front_page() ) {
										?><h1><?php } ?>
										<?php
										if ( get_option( SHORTNAME . '_logo_txt' ) ) {
											if ( get_bloginfo( 'name' ) ) {
												?><a href="<?php echo home_url() ?>"><span><?php bloginfo( 'name' ); ?></span></a><?php
											}
										} else {
											?>
											<a href="<?php echo home_url() ?>"><img src="<?php echo get_option( SHORTNAME . '_logo_custom' ); ?>" alt="<?php bloginfo( 'name' ); ?>" data-retina="<?php echo get_option( SHORTNAME . '_logo_custom_retina' ); ?>" /><span class="hidden"><?php bloginfo( 'name' ); ?></span></a>
										<?php } ?>
										<?php
										if ( is_front_page() ) {
										?></h1><?php } ?>
									<?php } ?>
                        </div>
						<div class="header-widget clearfix grid grid_7<?php
						if ( th_woocommerce_activated() && get_option( SHORTNAME . '_topline_cart' ) ) {
							echo (' topline-cart-indent');
						}
						?>">
									<?php dynamic_sidebar( 'header' ) ?>
                        </div>
						<?php
						if ( get_option( SHORTNAME . '_ribbon' ) ) {
						?>
                            <div class="ribbon_holder">
                                <span class="ribbon_bg"></span>
								<a href="<?php echo get_option( SHORTNAME . '_ribbon' ) ?>" class="ribbon"><span>+</span></a>
                            </div>
						<?php } ?>
                    </div>        
                </div>
            </div>
        </header>

        <section id="color_header" class="clearfix">
			<div class="mainmenu <?php echo (get_option( SHORTNAME . '_menu_left' )) ? 'menu_left' : ''; ?>"><div class="mainmenu_inner"><div class="row clearfix"><div>

							<?php
							wp_nav_menu( array( 'theme_location' => 'header-menu', 'container_class' => 'main_menu', 'menu_class' => 'sf-menu clearfix mobile-menu', 'fallback_cb' => '', 'container' => 'nav', 'link_before' => '', 'link_after' => '', 'items_wrap' => '<div id="menu-icon"><div><em></em><em></em><em></em></div>' . __( 'Navigation', 'churchope' ) . '</div><ul id="%1$s" class="%2$s">%3$s</ul>', 'walker' => new Walker_Nav_Menu_Sub() ) );
							?>

                        </div></div></div></div>    

			<?php get_template_part( 'title' ); ?>
        </section>  
        <section class="gray_line clearfix" id="title_sidebar"> 
            <div class="row">
				<div class="grid_12">
					<?php
					if ( is_singular() || (get_option( 'show_on_front' ) == 'page' && is_home()) ) {
						$curid = (get_option( 'show_on_front' ) == 'page' && is_home()) ? get_option( 'page_for_posts' ) : get_the_ID();

						if ( get_post_meta( $curid, SHORTNAME . '_title_sidebar', true ) ) {
							$sidebar = get_post_meta( $curid, SHORTNAME . '_title_sidebar', true );
							generated_dynamic_sidebar_th( $sidebar );
						}
					}
					?>
                </div>
			</div>
        </section>
        <div role="main" id="main">
