<?php
/*
  Template Name: RightSidebar
 */
?>
<?php get_header(); ?>
<div id="contentarea" class="row clearfix">
    <div class="main-content grid_8">
            <div class="grid_bg">    
		<?php get_template_part( 'loop' ); ?>
            </div>
        </div>
    <aside class="grid_4 right-sidebar">
		<?php do_action( 'th_sidebar_before' ); ?>
		<?php (get_post_meta( get_the_ID(), SHORTNAME . '_page_sidebar', true )) ? $sidebar = get_post_meta( get_the_ID(), SHORTNAME . '_page_sidebar', true ) : $sidebar = 'default-sidebar';
		generated_dynamic_sidebar_th( $sidebar ); ?>
		<?php do_action( 'th_sidebar_before' ); ?>
    </aside>
</div>
<?php get_footer(); ?>
