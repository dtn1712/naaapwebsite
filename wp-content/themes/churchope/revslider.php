<?php
global $post;
$pid = (isset( $post->ID )) ? $post->ID : null;

if ( is_home() ) {
	$pid = get_option( 'page_for_posts' );
}
	global $wp_query;
	$current_term = $wp_query->get_queried_object();


if ( is_post_type_archive( 'product' ) ) {
	if ( is_shop() ) {
		$pid = wc_get_page_id( 'shop' );
		$slider = get_post_meta( $pid, SHORTNAME . '_global_slider_alias', true );
	}
} // taxonomy page
elseif ( (is_tax() || is_tag() || is_category()) && $current_term && get_tax_meta( $current_term->term_id, SHORTNAME . '_global_slider_alias', true ) ) {
	$slider = get_tax_meta( $current_term->term_id, SHORTNAME . '_global_slider_alias', true );
} //post page
elseif ( ! is_tax() && ! is_tag() && ! is_category() && get_post_meta( $pid, SHORTNAME . '_post_slider', true ) ) {
	$slider = get_post_meta( $pid, SHORTNAME . '_global_slider_alias', true );
} elseif ( is_home() && ! get_option( 'page_on_front' ) ) {
	$slider = get_option( SHORTNAME . '_blog_revslider' );
} else {
	$slider = get_option( SHORTNAME . Admin_Theme_Item_Slideshow::SLIDER );
}
echo ($slider != '') ? do_shortcode( "[rev_slider $slider]" ) : '';
?>
