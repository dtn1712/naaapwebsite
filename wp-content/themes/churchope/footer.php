</div>

<?php if (is_front_page()): ?>
	<section class="gray_line clearfix" id="title_sidebar">
		<div class="row">
			<div class="grid_12">
				<?php
				$curid = (get_option( 'show_on_front' ) == 'page' && is_home()) ? get_option( 'page_for_posts' ) : get_the_ID();

				if ( get_post_meta( $curid, SHORTNAME . '_title_sidebar', true ) ) {
					$sidebar = get_post_meta( $curid, SHORTNAME . '_title_sidebar', true );
					generated_dynamic_sidebar_th( $sidebar );
				}
				?>
			</div>
		</div>
	</section>
<?php endif; ?>

<?php if(is_front_page()): ?>

	<div class="row">
		<div class="grid_12">
			<h1 style="color: #c62b02;font-family: 'Open Sans', Arial, Helvetica, sans-serif; font-weight: 600;line-height: 27px;">
				SPECIAL THANKS TO OUR SPONSORS
			</h1>

			<?php echo do_shortcode('[logo_carousel_slider]'); ?>
		</div>
	</div>

<?php endif; ?>

<footer>
	<?php
	if ( get_option( SHORTNAME . '_footer_widgets_enable' ) != '' ) {
		switch ( get_option( SHORTNAME . '_footer_widgets_columns' ) ) {
			case '1' :
				{
					$column_class = 'grid_12';
					break;
			}
			case '2' :
				{
					$column_class = 'grid_6';
					break;
			}
			case '3' :
				{
					$column_class = 'grid_4';
					break;
			}
			case '4' :
				{
					$column_class = 'grid_3';
					break;
			}
		}
		?>
        <section id="footer_widgets" class="clearfix row">
			<?php
			$i = 1;
			while ( $i <= (int) get_option( SHORTNAME . '_footer_widgets_columns' ) ) {
			?>
				<aside class="<?php echo $column_class ?>">
					<?php dynamic_sidebar( 'footer-' . $i ) ?>
                </aside>
				<?php
				$i++;
			}
			?>
            <div class="grid_12 dotted"></div>  
        </section> 
	<?php } ?>  
	<?php if ( has_nav_menu( 'footer-menu' ) || get_option( SHORTNAME . '_copyright' ) ) {
		?>
        <div class="clearfix row" id="copyright">
			<div class="grid_5"><p><?php echo stripslashes( get_option( SHORTNAME . '_copyright' ) ); ?></p></div>           
		<?php wp_nav_menu( array( 'theme_location' => 'footer-menu', 'container_class' => 'grid_7 clearfix', 'fallback_cb' => '', 'container' => 'nav' ) ); ?>
        </div>
	<?php } ?>

<?php echo stripslashes( get_option( SHORTNAME . '_GA' ) ); ?>
</footer>
<?php echo ((get_option( SHORTNAME . '_preview' ) && isset( $_SESSION[ SHORTNAME . '_boxed' ] ) && $_SESSION[ SHORTNAME . '_boxed' ] === '1') ? '</div>' : (get_option( SHORTNAME . '_boxed' )) ? '</div>' : ''); ?>
<?php wp_footer(); ?>
</body>
</html>
