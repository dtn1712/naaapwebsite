<?php

class Import_Theme_Revolution extends Import_Theme_Default
{

	function __construct( $type ) {

		parent::__construct( $type );
	}

	/**
	 * Path in theme with revolution slider import/export files
	 */
	const REVSLIDER_IMPORT_DIR = '/backend/dummy/revslides/';

	/**
	 * RevSlider import file extension
	 */
	const IMPORT_FILENAME_EXTENSION = 'zip';

	public function import_v1() {

		$slidesImportFileList = $this->getImportFilesList( get_template_directory() . self::REVSLIDER_IMPORT_DIR . '/v1' );

		if ( $slidesImportFileList ) {
			foreach ( $slidesImportFileList as $importFilePath ) {
				$slide = new Import_Theme_RevSlider();
				$slide->setImportFilePath( $importFilePath );
				$slide->import();
			}
		}
	}

	public function import_v2() {

		$slidesImportFileList = $this->getImportFilesList( get_template_directory() . self::REVSLIDER_IMPORT_DIR . '/v2' );

		if ( $slidesImportFileList ) {
			foreach ( $slidesImportFileList as $importFilePath ) {
				$slide = new Import_Theme_RevSlider();
				$slide->setImportFilePath( $importFilePath );
				$slide->import();
			}
		}
	}

	/**
	 * Get list of revslider import files in dir
	 * @param string $path path to dir
	 * @return array with file path
	 */
	private function getImportFilesList( $path ) {

		$dir = scandir( $path );
		$arrFiles = array();

		foreach ( $dir as $file ) {
			if ( $file != '.' && $file != '..' && pathinfo( $file, PATHINFO_EXTENSION ) == self::IMPORT_FILENAME_EXTENSION ) {
				$filepath = $path . DIRECTORY_SEPARATOR . $file;
				if ( is_file( $filepath ) ) {
					$arrFiles[] = $filepath;
				}
			}
		}
		return($arrFiles);
	}
}
