<?php

/**
 * Class for adding import from file functionality to Revolution slider importer
 */
class Import_Theme_RevSlider
{

	private $import_file_path = '';


	/**
	 * Method for create slider and update it with import data
	 */
	public function import() {

		if ( class_exists( 'RevSlider' ) ) {
			ob_start();
			$slider = new RevSlider();
			$slider->importSliderFromPost( true, true, $this->getImportFilePath() );
			ob_get_clean();
		}

		return false;
	}

	public function setImportFilePath( $path ) {

		$this->import_file_path = $path;
		return $this;
	}

	private function getImportFilePath() {

		return $this->import_file_path;
	}
}
