<?php

class Import_Theme_Menus extends Import_Theme_Default
{
	function __construct( $type ) {

		parent::__construct( $type );
	}
	public function import_v1() {

		global $wpdb;

		$table_db_name = $wpdb->prefix . 'terms';
		$rows = $wpdb->get_results( "SELECT * FROM $table_db_name where  name='Main' OR name='Footer Menu'", ARRAY_A );
		$menu_ids = array();

		foreach ( $rows as $row ) {
			$menu_ids[ $row['name'] ] = $row['term_id'];
		}

		$items = wp_get_nav_menu_items( $menu_ids['Main'] );

		foreach ( $items as $item ) {
			if ( $item->title == 'Home' ) {
				update_post_meta( $item->ID, '_menu_item_url', home_url() );
			}

			if ( ($item->title == 'Shop' || $item->title == 'Shop –  Sidebar') && th_woocommerce_activated() ) {
				update_post_meta( $item->ID, '_menu_item_object_id',wc_get_page_id( 'shop' ) );
				update_post_meta( $item->ID, '_menu_item_url',wc_get_page_permalink( 'shop' ) );
			}
		}

		set_theme_mod( 'nav_menu_locations', array_map( 'absint', array( 'header-menu' => $menu_ids['Main'], 'footer-menu' => $menu_ids['Footer Menu'] ) ) );
	}

	public function import_v2() {

		global $wpdb;

		$table_db_name = $wpdb->prefix . 'terms';
		$rows = $wpdb->get_results( "SELECT * FROM $table_db_name where  name='Main' OR name='Footer'", ARRAY_A );
		$menu_ids = array();

		foreach ( $rows as $row ) {
			$menu_ids[ $row['name'] ] = $row['term_id'];
		}

		$items = wp_get_nav_menu_items( $menu_ids['Main'] );

		foreach ( $items as $item ) {
			if ( $item->title == 'Home' ) {
				update_post_meta( $item->ID, '_menu_item_url', home_url() );
			}

			if ( ($item->title == 'Shop' || $item->title == 'Shop Page with Sidebar') && th_woocommerce_activated() ) {
				update_post_meta( $item->ID, '_menu_item_object_id',wc_get_page_id( 'shop' ) );
				update_post_meta( $item->ID, '_menu_item_url',wc_get_page_permalink( 'shop' ) );
			}
		}

		set_theme_mod( 'nav_menu_locations', array_map( 'absint', array( 'header-menu' => $menu_ids['Main'], 'footer-menu' => $menu_ids['Footer'] ) ) );
	}
}
?>
