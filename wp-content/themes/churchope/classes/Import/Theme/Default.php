<?php

abstract class Import_Theme_Default// implements Import_Theme_Item
{
	protected $install_type = '';

	public function __construct( $type ) {

		$this->setInstallType( $type );
	}

	private function setInstallType( $type ) {

		$this->install_type = $type;
	}

	protected function getInstallType() {

		return $this->install_type;
	}

	public function import() {

		switch ( $this->getInstallType() ) {
			case Import_Dummy::V1:
				$this->import_v1();
				break;
			case Import_Dummy::V2:
				$this->import_v2();
				break;
		}
	}

	abstract public function import_v1();
	abstract public function import_v2();

}
