<?php

class Import_Theme_Options extends Import_Theme_Default
{

	function __construct( $type ) {

		parent::__construct( $type );
	}

	public function import_v1() {

		update_option( 'shop_catalog_image_size', unserialize( 'a:3:{s:5:"width";s:3:"200";s:6:"height";s:3:"200";s:4:"crop";i:1;}' ) );

		update_option( 'ch_authorbox', '' );

		update_option( 'tax_meta_14', unserialize( 'a:7:{s:22:"ch_post_listing_layout";s:19:"layout_none_sidebar";s:23:"ch_post_listing_sidebar";s:5:"front";s:13:"ch_tax_slider";s:6:"jCycle";s:17:"ch_tax_slider_cat";s:9:"slideshow";s:19:"ch_tax_slider_count";s:1:"9";s:21:"ch_tax_slider_timeout";s:4:"6000";s:19:"ch_tax_slider_speed";s:4:"1000";}' ) );

		update_option( 'tax_meta_15', unserialize( 'a:6:{s:22:"ch_post_listing_layout";s:19:"layout_none_sidebar";s:23:"ch_post_listing_sidebar";s:5:"front";s:17:"ch_tax_slider_cat";s:13:"postslideshow";s:19:"ch_tax_slider_count";s:1:"4";s:21:"ch_tax_slider_timeout";s:4:"6000";s:19:"ch_tax_slider_speed";s:4:"1000";}' ) );

		update_option( 'tax_meta_16', unserialize( 'a:7:{s:22:"ch_post_listing_layout";s:20:"layout_right_sidebar";s:23:"ch_post_listing_sidebar";s:12:"postSidebar2";s:13:"ch_tax_slider";s:6:"jCycle";s:17:"ch_tax_slider_cat";s:13:"postslideshow";s:19:"ch_tax_slider_count";s:1:"4";s:21:"ch_tax_slider_timeout";s:4:"6000";s:19:"ch_tax_slider_speed";s:4:"1000";}' ) );

		update_option( 'tax_meta_26', unserialize( 'a:5:{s:23:"ch_post_listing_sidebar";s:5:"front";s:17:"ch_tax_slider_cat";s:13:"postslideshow";s:19:"ch_tax_slider_count";s:1:"4";s:21:"ch_tax_slider_timeout";s:4:"6000";s:19:"ch_tax_slider_speed";s:4:"1000";}' ) );

		update_option( 'tax_meta_27', unserialize( 'a:5:{s:23:"ch_post_listing_sidebar";s:5:"front";s:17:"ch_tax_slider_cat";s:13:"postslideshow";s:19:"ch_tax_slider_count";s:1:"4";s:21:"ch_tax_slider_timeout";s:4:"6000";s:19:"ch_tax_slider_speed";s:4:"1000";}' ) );

		update_option( 'tax_meta_28', unserialize( 'a:6:{s:9:"ch_layout";s:5:"Small";s:23:"ch_post_listing_sidebar";s:5:"front";s:17:"ch_tax_slider_cat";s:13:"postslideshow";s:19:"ch_tax_slider_count";s:1:"4";s:21:"ch_tax_slider_timeout";s:4:"6000";s:19:"ch_tax_slider_speed";s:4:"1000";}' ) );

		update_option( 'tax_meta_29', unserialize( 'a:5:{s:23:"ch_post_listing_sidebar";s:5:"front";s:17:"ch_tax_slider_cat";s:13:"postslideshow";s:19:"ch_tax_slider_count";s:1:"4";s:21:"ch_tax_slider_timeout";s:4:"6000";s:19:"ch_tax_slider_speed";s:4:"1000";}' ) );

		update_option( 'tax_meta_30', unserialize( 'a:5:{s:23:"ch_post_listing_sidebar";s:5:"front";s:17:"ch_tax_slider_cat";s:13:"postslideshow";s:19:"ch_tax_slider_count";s:1:"4";s:21:"ch_tax_slider_timeout";s:4:"6000";s:19:"ch_tax_slider_speed";s:4:"1000";}' ) );

		update_option( 'tax_meta_31', unserialize( 'a:2:{s:23:"ch_post_listing_sidebar";s:5:"front";s:17:"ch_tax_slider_cat";s:13:"postslideshow";}' ) );

		update_option( 'tax_meta_32', unserialize( 'a:5:{s:23:"ch_post_listing_sidebar";s:5:"front";s:17:"ch_tax_slider_cat";s:13:"postslideshow";s:19:"ch_tax_slider_count";s:1:"4";s:21:"ch_tax_slider_timeout";s:4:"6000";s:19:"ch_tax_slider_speed";s:4:"1000";}' ) );

		update_option( 'tax_meta_33', unserialize( 'a:2:{s:23:"ch_post_listing_sidebar";s:5:"front";s:17:"ch_tax_slider_cat";s:13:"postslideshow";}' ) );

		update_option( 'tax_meta_34', unserialize( 'a:2:{s:23:"ch_post_listing_sidebar";s:5:"front";s:17:"ch_tax_slider_cat";s:13:"postslideshow";}' ) );

		update_option( 'tax_meta_35', unserialize( 'a:7:{s:22:"ch_post_listing_layout";s:20:"layout_right_sidebar";s:23:"ch_post_listing_sidebar";s:4:"blog";s:13:"ch_tax_slider";s:6:"jCycle";s:17:"ch_tax_slider_cat";s:13:"postslideshow";s:19:"ch_tax_slider_count";s:1:"4";s:21:"ch_tax_slider_timeout";s:4:"6000";s:19:"ch_tax_slider_speed";s:4:"1000";}' ) );

		update_option( 'tax_meta_44', unserialize( 'a:11:{s:22:"ch_post_listing_layout";s:20:"layout_right_sidebar";s:23:"ch_post_listing_sidebar";s:5:"front";s:22:"ch_global_slider_alias";s:4:"main";s:17:"ch_tax_slider_cat";s:8:"contacts";s:19:"ch_tax_slider_count";s:1:"4";s:21:"ch_tax_slider_timeout";s:4:"6000";s:19:"ch_tax_slider_speed";s:4:"1000";s:20:"ch_term_header_color";s:1:"#";s:28:"ch_term_headerpattern_repeat";s:6:"repeat";s:23:"ch_term_headerpattern_x";s:1:"0";s:23:"ch_term_headerpattern_y";s:1:"0";}' ) );

		update_option( 'tax_meta_45', unserialize( 'a:8:{s:17:"ch_speaker_church";s:370:"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.";s:18:"ch_speaker_excerpt";s:91:"Sed ut perspiciatis unde omnis iste natus error sit voluptatem laudantium, totam rem ape...";s:14:"ch_speaker_ava";a:2:{s:2:"id";s:4:"3660";s:3:"src";s:74:"http://churchope.themoholics.com/wp-content/uploads/2014/12/speaker_04.jpg";}s:23:"ch_post_listing_sidebar";s:5:"front";s:20:"ch_term_header_color";s:1:"#";s:28:"ch_term_headerpattern_repeat";s:6:"repeat";s:23:"ch_term_headerpattern_x";s:1:"0";s:23:"ch_term_headerpattern_y";s:1:"0";}' ) );

		update_option( 'tax_meta_49', unserialize( 'a:8:{s:17:"ch_speaker_church";s:370:"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.";s:18:"ch_speaker_excerpt";s:91:"Sed ut perspiciatis unde omnis iste natus error sit voluptatem laudantium, totam rem ape...";s:14:"ch_speaker_ava";a:2:{s:2:"id";s:4:"3659";s:3:"src";s:74:"http://churchope.themoholics.com/wp-content/uploads/2014/12/speaker_03.jpg";}s:23:"ch_post_listing_sidebar";s:5:"front";s:20:"ch_term_header_color";s:1:"#";s:28:"ch_term_headerpattern_repeat";s:6:"repeat";s:23:"ch_term_headerpattern_x";s:1:"0";s:23:"ch_term_headerpattern_y";s:1:"0";}' ) );

		update_option( 'tax_meta_50', unserialize( 'a:8:{s:17:"ch_speaker_church";s:370:"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.";s:18:"ch_speaker_excerpt";s:91:"Sed ut perspiciatis unde omnis iste natus error sit voluptatem laudantium, totam rem ape...";s:14:"ch_speaker_ava";a:2:{s:2:"id";s:4:"3657";s:3:"src";s:74:"http://churchope.themoholics.com/wp-content/uploads/2014/12/speaker_01.jpg";}s:23:"ch_post_listing_sidebar";s:5:"front";s:20:"ch_term_header_color";s:1:"#";s:28:"ch_term_headerpattern_repeat";s:6:"repeat";s:23:"ch_term_headerpattern_x";s:1:"0";s:23:"ch_term_headerpattern_y";s:1:"0";}' ) );

		update_option( 'tax_meta_51', unserialize( 'a:8:{s:17:"ch_speaker_church";s:370:"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.";s:18:"ch_speaker_excerpt";s:91:"Sed ut perspiciatis unde omnis iste natus error sit voluptatem laudantium, totam rem ape...";s:14:"ch_speaker_ava";a:2:{s:2:"id";s:4:"3658";s:3:"src";s:74:"http://churchope.themoholics.com/wp-content/uploads/2014/12/speaker_02.jpg";}s:23:"ch_post_listing_sidebar";s:5:"front";s:20:"ch_term_header_color";s:1:"#";s:28:"ch_term_headerpattern_repeat";s:6:"repeat";s:23:"ch_term_headerpattern_x";s:1:"0";s:23:"ch_term_headerpattern_y";s:1:"0";}' ) );

		update_option( 'tax_meta_6', unserialize( 'a:6:{s:22:"ch_post_listing_layout";s:19:"layout_left_sidebar";s:23:"ch_post_listing_sidebar";s:4:"post";s:17:"ch_tax_slider_cat";s:13:"postslideshow";s:19:"ch_tax_slider_count";s:1:"4";s:21:"ch_tax_slider_timeout";s:4:"6000";s:19:"ch_tax_slider_speed";s:4:"1000";}' ) );

		update_option( 'tax_meta_8', unserialize( 'a:6:{s:22:"ch_post_listing_layout";s:19:"layout_left_sidebar";s:23:"ch_post_listing_sidebar";s:12:"postSidebar1";s:17:"ch_tax_slider_cat";s:13:"postslideshow";s:19:"ch_tax_slider_count";s:1:"4";s:21:"ch_tax_slider_timeout";s:4:"6000";s:19:"ch_tax_slider_speed";s:4:"1000";}' ) );

		update_option( 'tax_meta_9', unserialize( 'a:7:{s:22:"ch_post_listing_layout";s:20:"layout_right_sidebar";s:23:"ch_post_listing_sidebar";s:4:"blog";s:22:"ch_post_listing_number";s:1:"4";s:17:"ch_tax_slider_cat";s:13:"postslideshow";s:19:"ch_tax_slider_count";s:1:"4";s:21:"ch_tax_slider_timeout";s:4:"6000";s:19:"ch_tax_slider_speed";s:4:"1000";}' ) );

		update_option( 'ch_sidebar_generator', unserialize( 'a:24:{i:1;a:1:{s:4:"name";s:5:"front";}i:2;a:1:{s:4:"name";s:8:"features";}i:3;a:1:{s:4:"name";s:4:"blog";}i:4;a:1:{s:4:"name";s:4:"post";}i:5;a:1:{s:4:"name";s:12:"postSidebar1";}i:6;a:1:{s:4:"name";s:12:"postSidebar2";}i:7;a:1:{s:4:"name";s:12:"postSidebar3";}i:8;a:1:{s:4:"name";s:9:"portfolio";}i:9;a:1:{s:4:"name";s:13:"portfoliopost";}i:10;a:1:{s:4:"name";s:10:"Shortcodes";}i:11;a:1:{s:4:"name";s:13:"lotsofwidgets";}i:12;a:1:{s:4:"name";s:12:"rightsidebar";}i:13;a:1:{s:4:"name";s:6:"buynow";}i:14;a:1:{s:4:"name";s:8:"contacts";}i:15;a:1:{s:4:"name";s:6:"social";}i:16;a:1:{s:4:"name";s:9:"contacts2";}i:17;a:1:{s:4:"name";s:4:"docs";}i:18;a:1:{s:4:"name";s:12:"testimonials";}i:19;a:1:{s:4:"name";s:4:"test";}i:20;a:1:{s:4:"name";s:7:"sermons";}i:21;a:1:{s:4:"name";s:11:"sermon post";}i:22;a:1:{s:4:"name";s:8:"schedule";}i:23;a:1:{s:4:"name";s:8:"speakers";}i:24;a:1:{s:4:"name";s:4:"Shop";}}' ) );

		update_option( 'ch_boxedbackground', '#F1F1F1' );

		update_option( 'ch_boxedpattern_repeat', 'repeat' );

		update_option( 'ch_cart_ribbon_color', '#c62b02' );

		update_option( 'ch_category_font_size', '16px' );

		update_option( 'ch_category_font_weight', '400' );

		update_option( 'ch_category_pricecolor', '#c62c02' );

		update_option( 'ch_category_special_pricecolor', '#758d02' );

		update_option( 'ch_content_font_size', '12px' );

		update_option( 'ch_content_line_height', '18px' );

		update_option( 'ch_content_width', '960px' );

		update_option( 'ch_copyright', 'Churchope 2015 © <a href="http://themoholics.com">Premium WordPress Themes</a> by Themoholics' );

		update_option( 'ch_customcss', 'pre { color: #343434; } .sf-menu > li > a { font-size: 15px; } .widget_feedburner input { margin: 12px 0px 0px; } .widget_feedburner button{ padding: 8px 17px 7px; margin-left:0px; } ' );

		update_option( 'ch_custom_gallery_icons', '' );

		update_option( 'ch_events_layout', 'none' );

		update_option( 'ch_events_listing_layout', 'none' );

		update_option( 'ch_events_listing_sidebar', 'front' );

		update_option( 'ch_events_sidebar', 'front' );

		update_option( 'ch_excerpt', '' );

		update_option( 'ch_favicon', 'http://churchope.themoholics.com/wp-content/themes/churchope/images/favicon.ico' );

		update_option( 'ch_fontstyle', 'italic' );

		update_option( 'ch_footeractivemenucolor', '#656565' );

		update_option( 'ch_footerbgcolor', '#fafafa' );

		update_option( 'ch_footercopyrightcolor', '#afafaf' );

		update_option( 'ch_footerheadingscolor', '#545454' );

		update_option( 'ch_footerlinkscolor', '#c62b02' );

		update_option( 'ch_footertextcolor', '#919191' );

		update_option( 'ch_footer_widgets_columns', '4' );

		update_option( 'ch_footer_widgets_enable', '1' );

		update_option( 'ch_galleries_listing_layout', 'none' );

		update_option( 'ch_galleries_listing_sidebar', '' );

		update_option( 'ch_gallery_layout', 'none' );

		update_option( 'ch_gallery_sidebar', '' );

		update_option( 'ch_gfont', '[{"family":"Open Sans","variants":["600","regular","italic","700"],"subsets":["latin"]}]' );

		update_option( 'ch_gfont_menu', '[{"family":"Open Sans","variants":["600","regular","italic","700"],"subsets":["latin"]}]' );

		update_option( 'ch_global_slider', 'Disable' );

		update_option( 'ch_global_slider_autoscroll', '1' );

		update_option( 'ch_global_slider_cat', 'slideshow' );

		update_option( 'ch_global_slider_count', '9' );

		update_option( 'ch_global_slider_fixedheight', '' );

		update_option( 'ch_global_slider_navigation', '1' );

		update_option( 'ch_global_slider_pause', '1' );

		update_option( 'ch_global_slider_speed', '1000' );

		update_option( 'ch_global_slider_timeout', '6000' );

		update_option( 'ch_headerbgcolor', '#261c1e' );

		update_option( 'ch_headerpattern', 'http://churchope.themoholics.com/wp-content/themes/churchope/images/bg_header_pattern.png' );

		update_option( 'ch_headerpattern_repeat', 'repeat' );

		update_option( 'ch_headerpattern_x', '0' );

		update_option( 'ch_headerpattern_y', '0' );

		update_option( 'ch_headertextcolor', '#eeeeee' );

		update_option( 'ch_header_ribbon_color', '#94b301' );

		update_option( 'ch_headingscolor', '#545454' );

		update_option( 'ch_is_writable_style_file', '1' );

		update_option( 'ch_linkscolor', '#c62b02' );

		update_option( 'ch_logo_custom', 'http://churchope.themoholics.com/wp-content/themes/churchope/images/logo.png' );

		update_option( 'ch_logo_custom_retina', 'http://churchope.themoholics.com/wp-content/themes/churchope/images/retina/logo@2x.png' );

		update_option( 'ch_maps_language', '' );

		update_option( 'ch_maps_tooltips', '1' );

		update_option( 'ch_maps_types_switch', '1' );

		update_option( 'ch_maps_weel_zoom', '' );

		update_option( 'ch_menubgcolor', '#c62b02' );

		update_option( 'ch_menupattern', 'http://churchope.themoholics.com/wp-content/themes/churchope/images/menu_pattern.png' );

		update_option( 'ch_menupattern_repeat', 'repeat' );

		update_option( 'ch_menupattern_x', '0' );

		update_option( 'ch_menupattern_y', '0' );

		update_option( 'ch_menutextcolor', '#ffffff' );

		update_option( 'ch_menu_color', '#ffffff' );

		update_option( 'ch_menu_font_size', '16px' );

		update_option( 'ch_menu_letter_spacing', '-1px' );

		update_option( 'ch_new_label_bgcolor', '#c6320d' );

		update_option( 'ch_new_label_color', '#ffffff' );

		update_option( 'ch_new_label_days', '500' );

		update_option( 'ch_outofstok_label_bgcolor', '#cfcfcf' );

		update_option( 'ch_outofstok_label_color', '#ffffff' );

		update_option( 'ch_post_layout', 'right' );

		update_option( 'ch_post_listing_layout', 'right' );

		update_option( 'ch_post_listing_sidebar', '3' );

		update_option( 'ch_post_sidebar', '1' );

		update_option( 'ch_products_listing_layout', 'right' );

		update_option( 'ch_products_listing_sidebar', '24' );

		update_option( 'ch_product_font_size', '26px' );

		update_option( 'ch_product_font_weight', '400' );

		update_option( 'ch_product_instock_color', '#758d02' );

		update_option( 'ch_product_layout', 'none' );

		update_option( 'ch_product_outofstock_color', '#c62c02' );

		update_option( 'ch_product_pricecolor', '#545454' );

		update_option( 'ch_product_sidebar', '1' );

		update_option( 'ch_product_special_pricecolor', '#758d02' );

		update_option( 'ch_ribbon', 'http://themoholics.com' );

		update_option( 'ch_sale_label_bgcolor', '#94b301' );

		update_option( 'ch_sale_label_color', '#ffffff' );

		update_option( 'ch_sermon', 'th_sermon' );

		update_option( 'ch_sermons_layout', 'right' );

		update_option( 'ch_sermons_listing_layout', 'none' );

		update_option( 'ch_sermons_listing_sidebar', '20' );

		update_option( 'ch_sermons_sidebar', '21' );

		update_option( 'ch_sermon_cat', 'th_sermon_cat' );

		update_option( 'ch_sermon_speaker', 'th_sermon_speaker' );

		update_option( 'ch_show_top_line', '1' );

		update_option( 'ch_slug_event', 'th_event' );

		update_option( 'ch_slug_event_cat', 'th_event_cat' );

		update_option( 'ch_slug_gallery', 'th_gallery' );

		update_option( 'ch_slug_gallery_cat', 'th_gallery_cat' );

		update_option( 'ch_slug_slideshow', 'th_slideshow' );

		update_option( 'ch_slug_slideshow_cat', 'th_slideshow_cat' );

		update_option( 'ch_slug_testimonial', 'th_testimonial' );

		update_option( 'ch_slug_testimonial_cat', 'th_testimonial_cat' );

		update_option( 'ch_testimonials_listing_layout', 'none' );

		update_option( 'ch_testimonials_listing_sidebar', '' );

		update_option( 'ch_testimonial_layout', 'none' );

		update_option( 'ch_testimonial_sidebar', '' );

		update_option( 'ch_textcolor', '#797979' );

		update_option( 'ch_topline_backgroundcolor', '#191214' );

		update_option( 'ch_topline_cart', '1' );

		update_option( 'ch_topline_headingscolor', '#ffffff' );

		update_option( 'ch_topline_linkscolor', '#ffffff' );

		update_option( 'ch_topline_login', '1' );

		update_option( 'ch_topline_textcolor', '#797979' );

		update_option( 'ch_topline_tinymce', 'You can add any content here! Call us now 1 -800-123-1245' );

		update_option( 'ch_widget_font_size', '12px' );

		update_option( 'ch_widget_pricecolor', '#c62c02' );

		update_option( 'ch_widget_special_pricecolor', '#758d02' );

		update_option( 'ch_woo_listing_per_page', '12' );

		update_option( 'show_on_front', 'page' );
		$blog = get_page_by_title( 'News' );
		update_option( 'page_for_posts', $blog->ID );
		$home = get_page_by_title( 'FrontPage' );
		update_option( 'page_on_front', $home->ID );
	}

	public function import_v2() {

		update_option( 'shop_catalog_image_size', unserialize( 'a:3:{s:5:"width";s:3:"260";s:6:"height";s:3:"320";s:4:"crop";i:1;}' ) );
		update_option( 'shop_single_image_size', unserialize( 'a:3:{s:5:"width";s:3:"600";s:6:"height";s:3:"760";s:4:"crop";i:1;}' ) );
		update_option( 'shop_thumbnail_image_size', unserialize( 'a:3:{s:5:"width";s:3:"140";s:6:"height";s:3:"180";s:4:"crop";i:1;}' ) );

		update_option( 'ch_authorbox', '1' );

		update_option( 'ch_sidebar_generator', unserialize( 'a:9:{i:1;a:1:{s:4:"name";s:2:"n1";}i:2;a:1:{s:4:"name";s:4:"Shop";}i:3;a:1:{s:4:"name";s:4:"blog";}i:4;a:1:{s:4:"name";s:4:"post";}i:5;a:1:{s:4:"name";s:7:"product";}i:6;a:1:{s:4:"name";s:10:"Newsletter";}i:7;s:0:"";i:8;a:1:{s:4:"name";s:4:"home";}i:9;a:1:{s:4:"name";s:15:"home_additional";}}' ) );

		update_option( 'ch_blog_revslider', '' );

		update_option( 'ch_blog_slider', 'Use global' );

		update_option( 'ch_blog_slider_autoplay', '' );

		update_option( 'ch_blog_slider_cat', '' );

		update_option( 'ch_blog_slider_count', '4' );

		update_option( 'ch_blog_slider_height', '' );

		update_option( 'ch_blog_slider_nav', '' );

		update_option( 'ch_blog_slider_padding', '' );

		update_option( 'ch_blog_slider_pause', '' );

		update_option( 'ch_blog_slider_speed', '1000' );

		update_option( 'ch_blog_slider_timeout', '6000' );

		update_option( 'ch_blog_title', '' );

		update_option( 'ch_boxed', '' );

		update_option( 'ch_boxedbackground', '#F1F1F1' );

		update_option( 'ch_boxedpattern', '' );

		update_option( 'ch_boxedpattern_repeat', 'repeat' );

		update_option( 'ch_boxedpattern_x', '0' );

		update_option( 'ch_boxedpattern_y', '0' );

		update_option( 'ch_captcha_private_key', '' );

		update_option( 'ch_captcha_public_key', '' );

		update_option( 'ch_cart_ribbon_color', '#ff614d' );

		update_option( 'ch_category_font_size', '' );

		update_option( 'ch_category_font_weight', '' );

		update_option( 'ch_category_pricecolor', '#ff614d' );

		update_option( 'ch_category_special_pricecolor', '#758d02' );

		update_option( 'ch_content_font_size', '14px' );

		update_option( 'ch_content_line_height', '22px' );

		update_option( 'ch_content_width', '1280px' );

		update_option( 'ch_copyright', 'Churchope 2015 © <a href="http://themoholics.com/">Premium WordPress Themes by Themoholics</a>' );

		update_option( 'ch_customcss', '.header-widget .widget-title a{ letter-spacing: 0; } header .widget-title a{ text-transform: none; } .logo img{ margin:0; } .ribbon{ border-top: 16px solid #ff614d; border-left: 16px solid #ff614d; border-right: 17px solid #ff614d; } .sf-menu>li>a{ text-shadow:none !important; font-size:14px !important; font-weight:800 !important; } .page-id-68 .sf-menu>li>a{ color:#ffffff !important; font-weight:600 !important; } .page-id-505 .sf-menu>li>a{ color:#ffffff !important; font-weight:600 !important; }' );

		update_option( 'ch_custom_gallery_icons', '' );

		update_option( 'ch_disable_cats', '' );

		update_option( 'ch_disable_tags', '' );

		update_option( 'ch_envato_api', '' );

		update_option( 'ch_envato_nick', '' );

		update_option( 'ch_envato_skip_backup', '' );

		update_option( 'ch_events_layout', 'none' );

		update_option( 'ch_events_listing_layout', 'none' );

		update_option( 'ch_events_listing_sidebar', '' );

		update_option( 'ch_events_sidebar', '' );

		update_option( 'ch_event_cat_order', 'DESC' );

		update_option( 'ch_excerpt', '' );

		update_option( 'ch_favicon', 'http://churchope.themoholics.com/v1/wp-content/themes/churchope/images/favicon.ico' );

		update_option( 'ch_fontawesomedisable', '' );

		update_option( 'ch_footeractivemenucolor', '#ff614d' );

		update_option( 'ch_footerbgcolor', '#fafafa' );

		update_option( 'ch_footercopyrightcolor', '#afafaf' );

		update_option( 'ch_footerheadingscolor', '#545454' );

		update_option( 'ch_footerlinkscolor', '#ff614d' );

		update_option( 'ch_footertextcolor', '#919191' );

		update_option( 'ch_footer_widgets_columns', '1' );

		update_option( 'ch_footer_widgets_enable', '1' );

		update_option( 'ch_GA', '' );

		update_option( 'ch_galleries_listing_layout', 'none' );

		update_option( 'ch_galleries_listing_sidebar', '' );

		update_option( 'ch_gallery_layout', 'none' );

		update_option( 'ch_gallery_sidebar', '' );

		update_option( 'ch_gfont', '[{"family":"Open Sans","variants":["300italic","300","regular","italic","600","600italic","700","700italic","800","800italic"],"subsets":[]}]' );

		update_option( 'ch_gfontdisable', '' );

		update_option( 'ch_gfont_menu', '[{"family":"Open Sans","variants":["700","800","600","regular"],"subsets":[]}]' );

		update_option( 'ch_global_slider', 'Disable' );

		update_option( 'ch_global_slider_alias', '' );

		update_option( 'ch_global_slider_autoscroll', '' );

		update_option( 'ch_global_slider_cat', '' );

		update_option( 'ch_global_slider_count', '4' );

		update_option( 'ch_global_slider_fixedheight', '' );

		update_option( 'ch_global_slider_navigation', '' );

		update_option( 'ch_global_slider_padding', '' );

		update_option( 'ch_global_slider_pause', '' );

		update_option( 'ch_global_slider_speed', '1000' );

		update_option( 'ch_global_slider_timeout', '6000' );

		update_option( 'ch_headerbgcolor', '#261c1e' );

		update_option( 'ch_headerpattern_repeat', 'repeat' );

		update_option( 'ch_headerpattern_x', '0' );

		update_option( 'ch_headerpattern_y', '0' );

		update_option( 'ch_headertextcolor', '#eeeeee' );

		update_option( 'ch_header_ribbon_color', '#ff614d' );

		update_option( 'ch_headingscolor', '#545454' );

		update_option( 'ch_hidedate', '' );

		update_option( 'ch_hidethumb', '' );

		update_option( 'ch_is_writable_style_file', '1' );

		update_option( 'ch_linkscolor', '#ff614d' );

		update_option( 'ch_logo_custom', 'http://churchope.themoholics.com/v1/wp-content/uploads/2015/09/logo_church.png' );

		update_option( 'ch_logo_custom_retina', 'http://churchope.themoholics.com/v1/wp-content/themes/churchope/images/retina/logo@2x.png' );

		update_option( 'ch_logo_txt', '' );

		update_option( 'ch_mailchimp_key', '' );

		update_option( 'ch_maps_language', '' );

		update_option( 'ch_maps_tooltips', 'true' );

		update_option( 'ch_maps_types_switch', 'true' );

		update_option( 'ch_maps_weel_zoom', '' );

		update_option( 'ch_menubgcolor', '#f7f2f4' );

		update_option( 'ch_menupattern', 'http://churchope.themoholics.com/v1/wp-content/uploads/2015/10/bg_pattern_1.jpg' );

		update_option( 'ch_menupattern_repeat', 'repeat-x' );

		update_option( 'ch_menupattern_x', '0' );

		update_option( 'ch_menupattern_y', '0' );

		update_option( 'ch_menutextcolor', '#433b3b' );

		update_option( 'ch_menu_color', '#261c1e' );

		update_option( 'ch_menu_font_size', '14px' );

		update_option( 'ch_menu_left', '' );

		update_option( 'ch_menu_letter_spacing', '-1px' );

		update_option( 'ch_new_label', '1' );

		update_option( 'ch_new_label_bgcolor', '#c6320d' );

		update_option( 'ch_new_label_color', '#ffffff' );

		update_option( 'ch_new_label_days', '' );

		update_option( 'ch_outofstok_label_bgcolor', '#eaeaeb' );

		update_option( 'ch_outofstok_label_color', '#261c1e' );

		update_option( 'ch_post_layout', 'none' );

		update_option( 'ch_post_listing_layout', 'right' );

		update_option( 'ch_post_listing_sidebar', '3' );

		update_option( 'ch_post_sidebar', '1' );

		update_option( 'ch_products_listing_layout', 'right' );

		update_option( 'ch_products_listing_sidebar', '2' );

		update_option( 'ch_product_font_size', '' );

		update_option( 'ch_product_font_weight', '' );

		update_option( 'ch_product_instock_color', '#758d02' );

		update_option( 'ch_product_layout', 'none' );

		update_option( 'ch_product_outofstock_color', '#ff614d' );

		update_option( 'ch_product_pricecolor', '#ff614d' );

		update_option( 'ch_product_sidebar', '1' );

		update_option( 'ch_product_special_pricecolor', '#758d02' );

		update_option( 'ch_responsive', '' );

		update_option( 'ch_ribbon', 'http://themoholics.com' );

		update_option( 'ch_round_labels', '1' );

		update_option( 'ch_sale_label_bgcolor', '#94b301' );

		update_option( 'ch_sale_label_color', '#ffffff' );

		update_option( 'ch_sermon', 'th_sermon' );

		update_option( 'ch_sermons_excerpt', '' );

		update_option( 'ch_sermons_hidedate', '' );

		update_option( 'ch_sermons_hidethumb', '' );

		update_option( 'ch_sermons_layout', 'none' );

		update_option( 'ch_sermons_listing_layout', 'none' );

		update_option( 'ch_sermons_listing_sidebar', '' );

		update_option( 'ch_sermons_sidebar', '' );

		update_option( 'ch_sermon_cat', 'th_sermon_cat' );

		update_option( 'ch_sermon_speaker', 'th_sermon_speaker' );

		update_option( 'ch_show_top_line', '' );

		update_option( 'ch_slug_event', 'th_event' );

		update_option( 'ch_slug_event_cat', 'th_event_cat' );

		update_option( 'ch_slug_gallery', 'th_gallery' );

		update_option( 'ch_slug_gallery_cat', 'th_gallery_cat' );

		update_option( 'ch_slug_testimonial', 'th_testimonial' );

		update_option( 'ch_slug_testimonial_cat', 'th_testimonial_cat' );

		update_option( 'ch_testimonials_listing_layout', 'none' );

		update_option( 'ch_testimonials_listing_sidebar', '' );

		update_option( 'ch_testimonial_layout', 'none' );

		update_option( 'ch_testimonial_sidebar', '' );

		update_option( 'ch_textcolor', '#797979' );

		update_option( 'ch_topline_backgroundcolor', '#181112' );

		update_option( 'ch_topline_cart', '1' );

		update_option( 'ch_topline_headingscolor', '#ffffff' );

		update_option( 'ch_topline_linkscolor', '#ffffff' );

		update_option( 'ch_topline_login', '1' );

		update_option( 'ch_topline_textcolor', '#797979' );

		update_option( 'ch_topline_tinymce', 'Call Us Now - 1 800 123 4567&nbsp; &nbsp;| &nbsp;&nbsp;<a href=="#">Need Help?</a>&nbsp;&nbsp; |&nbsp;&nbsp; <a href=="#">FAQ</a>' );

		update_option( 'ch_uppercase_disable', '' );

		update_option( 'ch_widget_font_size', '' );

		update_option( 'ch_widget_pricecolor', '#ff614d' );

		update_option( 'ch_widget_special_pricecolor', '#758d02' );

		update_option( 'ch_woo_listing_per_page', '20' );

		update_option( 'show_on_front', 'page' );
		$blog = get_page_by_title( 'Blog' );
		update_option( 'page_for_posts', $blog->ID );
		$home = get_page_by_title( 'Home' );
		update_option( 'page_on_front', $home->ID );
	}
}

?>
