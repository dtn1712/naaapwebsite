<?php

class Sidebar_Generator
{

	function sidebar_generator() {

		add_action( 'init', array( 'Sidebar_Generator', 'init' ) );
	}

	static function init() {

		// go through each sidebar and register it
		$sidebars = Sidebar_Generator::get_sidebars();

		if ( is_array( $sidebars ) ) {
			foreach ( $sidebars as $sidebar_id => $sidebar ) {
				if ( is_array( $sidebar ) && $sidebar['name'] ) {
					$sidebar_class = Sidebar_Generator::name_to_class( $sidebar['name'] );
					register_sidebar(array(
						'name' => $sidebar['name'],
						'id' => "th_sidebar-$sidebar_id",
						'before_widget' => '<div id="%1$s" class="widget ' . $sidebar_class . ' %2$s">',
						'after_widget' => '</div>',
						'before_title' => '<h3 class="widget-title">',
						'after_title' => '</h3>',
					));
				}
			}
		}
	}

	/**
	 * called by the action get_sidebar. this is what places this into the theme
	 */
	static function get_sidebar( $index ) {
		if ( 'default-sidebar' === $index ) {
		    dynamic_sidebar( $index );
		} else {
			dynamic_sidebar( 'th_sidebar-'.$index );}
	}

	static function db_compatibility( $sidebars ) {

		if ( is_array( $sidebars ) ) {
			$db_updated = false;

			foreach ( $sidebars as $sidebar_class => $sidebar ) {
				if ( is_array( $sidebar ) ) {
					$db_updated = true;
				}
			}

			if ( ! $db_updated ) {
				global $wpdb;
				$i = 1;
				$new_sidebars = array();
				foreach ( $sidebars as $sidebar_class => $sidebar ) {
					if ( $sidebar ) {
						$new_sidebars[ $i ] = array( 'name' => $sidebar );

						$sidebar_meta = $wpdb->get_results( "SELECT post_id, meta_key FROM $wpdb->postmeta WHERE meta_value = '$sidebar' AND meta_key LIKE  '%_sidebar'", ARRAY_A );

						if ( is_array( $sidebar_meta ) ) {
							foreach ( $sidebar_meta as $key => $value ) {
								update_post_meta( $value['post_id'], $value['meta_key'], $i );
							}
						}
					}

					$i++;
				}

				$sidebars = $new_sidebars;
				update_option( SHORTNAME . '_sidebar_generator', $sidebars );
			}
		}

		return $sidebars;
	}

	/**
	 * gets the generated sidebars
	 */
	static function get_sidebars() {

		$sidebars = Sidebar_Generator::db_compatibility( get_option( SHORTNAME . '_sidebar_generator' ) );
		return $sidebars;
	}

	static function name_to_class( $name ) {

		return sanitize_title( $name );
		// $class = str_replace(array(' ',',','.','"',"'",'/',"\\",'+','=',')','(','*','&','^','%','$','#','@','!','~','`','<','>','?','[',']','{','}','|',':',),'',$name);
		// return $class;
	}
}

$sbg = new Sidebar_Generator;

function generated_dynamic_sidebar_th( $index ) {

	Sidebar_Generator::get_sidebar( $index );
	return true;
}

?>
