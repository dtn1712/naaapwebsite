<?php

/**
 * Class for Sidebar Html element
 */
class Admin_Theme_Element_Sidebar extends Admin_Theme_Menu_Element
{

	protected $option = array(
		'type' => Admin_Theme_Menu_Element::TYPE_SIDEBAR,
	);

	public function render() {

		$checked = '';
		ob_start();
		echo $this->getElementHeader();
		?>
		<input size="<?php echo $this->size; ?>" name="<?php echo $this->id; ?>" id="<?php echo $this->id; ?>" type="text" value="" /><br><br>

		<input name="save_options" type="submit" value="<?php _e( 'Add sidebar', 'churchope' ); ?>" class="button" />
		<?php
		echo $this->getElementFooter();

		$html = ob_get_clean();
		return $html;
	}

	public function save() {

		$get_sidebar_options = Sidebar_Generator::get_sidebars();
		$sidebar_name = (isset( $_POST[ $this->id ] ) ? $_POST[ $this->id ] : null);
		$sidebar_name = str_replace( array( "\n", "\r", "\t" ), '', $sidebar_name );

		$sidebar_id = Sidebar_Generator::name_to_class( $sidebar_name );
		if ( $sidebar_id == '' ) {
			$options_sidebar = $get_sidebar_options;
		} else {
			if ( is_array( $get_sidebar_options ) ) {
				$get_sidebar_options[ max( array_keys( $get_sidebar_options ) ) + 1 ] = array( 'name' => $sidebar_name );
				$options_sidebar = $get_sidebar_options;
			} else {
				$options_sidebar[1] = array( 'name' => $sidebar_name );
			}
		}

		update_option( SHORTNAME . '_sidebar_generator', $options_sidebar );
	}
}
?>
