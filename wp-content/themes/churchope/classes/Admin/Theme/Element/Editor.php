<?php

/**
 * Class for Text Html element
 */
class Admin_Theme_Element_Editor extends Admin_Theme_Menu_Element
{

	protected $option = array(
		'type' => Admin_Theme_Menu_Element::TYPE_TEXT,
	);

	public function render() {

		ob_start();
		echo $this->getElementHeader();
		wp_editor($this->getCurrentValue(), $this->getId(), array(
			'media_buttons' => false,
			'tinymce' => array(
			'toolbar3' => '',
			),
		));
		echo $this->getElementFooter();

		$html = ob_get_clean();
		return $html;
	}

	private function getCurrentValue() {

		return get_option( $this->getId() );
	}

	public function add_customize_control( $wp_customize ) {

		$wp_customize->add_control($this->getId(), array(
			'label' => $this->getName(),
			'section' => $this->getCustomizeSection(),
			'settings' => $this->getId(),
			'type' => 'textarea',
		));
	}
}

?>
