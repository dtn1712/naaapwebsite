<?php

/**
 * 'Header' admin menu page
 */
class Admin_Theme_Item_TopLine extends Admin_Theme_Menu_Item
{

	public function __construct( $parent_slug = '' ) {

		$this->setPageTitle( 'Top Line' );
		$this->setMenuTitle( 'Top Line' );
		$this->setCapability( 'administrator' );
		$this->setMenuSlug( SHORTNAME . '_topline' );
		$this->setIsCustomize( true );
		parent::__construct( $parent_slug );

		$this->init();
	}

	public function init() {

		$option = new Admin_Theme_Element_Pagetitle();
		$option->setName( 'Top Line Settings' );
		$this->addOption( $option );
		$option = null;

		$option = new Admin_Theme_Element_Checkbox();
		$option->setName( 'Show Top Line' )
				->setDescription( 'Check to show Top Line' )
				->setId( SHORTNAME . '_show_top_line' )
				->setStd( '' )
				->setCustomized();
		$this->addOption( $option );
		$option = null;

				$option = new Admin_Theme_Element_Colorchooser();
		$option->setName( __( 'Top Line background color', 'churchope' ) )
				->setDescription( __( 'Please select your custom color for all elements on light background.', 'churchope' ) )
				->setId( SHORTNAME . '_topline_backgroundcolor' )
				->setStd( '#000000' );
		$this->addOption( $option );

				$option = new Admin_Theme_Element_Colorchooser();
		$option->setName( __( 'Top Line text color', 'churchope' ) )
				->setDescription( __( 'Please select your custom color for all elements on light background.', 'churchope' ) )
				->setId( SHORTNAME . '_topline_textcolor' )
				->setStd( '#797979' );
		$this->addOption( $option );

		$option = new Admin_Theme_Element_Colorchooser();
		$option->setName( __( 'Top Line headings color', 'churchope' ) )
				->setDescription( __( 'Please select your custom color for all elements on dark background (footer area).', 'churchope' ) )
				->setId( SHORTNAME . '_topline_headingscolor' )
				->setStd( '#ffffff' );
		$this->addOption( $option );

		$option = new Admin_Theme_Element_Colorchooser();
		$option->setName( __( 'Top Line links color', 'churchope' ) )
				->setDescription( __( 'Please select your custom color for all elements on dark background (footer area).', 'churchope' ) )
				->setId( SHORTNAME . '_topline_linkscolor' )
				->setStd( '#ffffff' );
		$this->addOption( $option );

		$option = new Admin_Theme_Element_Editor();
		$option->setName( 'Top line custom content' )
				->setDescription( 'You can use custom content' )
				->setId( SHORTNAME . '_topline_tinymce' )
				->setCustomized();
		$this->addOption( $option );
		$option = null;

		if ( th_woocommerce_activated() ) {

			$option = new Admin_Theme_Element_Checkbox();
			$option->setName( 'Show login link' )
				->setDescription( 'Check to show login link in top line' )
				->setId( SHORTNAME . '_topline_login' )
				->setStd( '' )
				->setCustomized();
			$this->addOption( $option );
			$option = null;

		}
	}
}

?>
