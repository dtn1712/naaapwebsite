<?php

/**
 * 'WooCommerce' admin menu page
 */
class Admin_Theme_Item_Woocommerce extends Admin_Theme_Menu_Item
{

	/**
	 * prefix of file icons option
	 */
	public function __construct( $parent_slug = '' ) {

		$this->setPageTitle( __( 'WooCommerce General options', 'churchope' ) );
		$this->setMenuTitle( __( 'WooCommerce', 'churchope' ) );
		$this->setCapability( 'administrator' );
		$this->setMenuSlug( SHORTNAME . '_woocommerce' );
		$this->setIsCustomize( true );
		parent::__construct( $parent_slug );
		$this->init();
	}

	public function init() {

		$option = new Admin_Theme_Element_Pagetitle();
		$option->setName( __( 'WooCommerce General Options', 'churchope' ) );
		$this->addOption( $option );
		$option = null;

		$option = new Admin_Theme_Element_Checkbox();
		$option->setDescription( __( 'Use product labels round style', 'churchope' ) )
				->setName( __( 'Use product labels round style', 'churchope' ) )
				->setCustomized()
				->setId( SHORTNAME . '_round_labels' );
		$this->addOption( $option );
		$option = null;

		$option = new Admin_Theme_Element_Separator();
		$this->addOption( $option );
		$option = null;

		$option = new Admin_Theme_Element_Checkbox();
		$option->setDescription( __( 'Hide New product label', 'churchope' ) )
				->setName( __( 'Hide New product label', 'churchope' ) )
				->setCustomized()
				->setId( SHORTNAME . '_new_label' );
		$this->addOption( $option );
		$option = null;

		$option = new Admin_Theme_Element_Text();
		$option->setName( __( 'Product Newness by days', 'churchope' ) )
				->setDescription( __( 'Product Newness by days', 'churchope' ) )
				->setId( SHORTNAME . '_new_label_days' )
				->setStd( '30' )
				->setCustomized();
		$this->addOption( $option );
		$option = null;

		$option = new Admin_Theme_Element_Colorchooser();
		$option->setName( __( 'New label text color', 'churchope' ) )
				->setDescription( __( 'New label text color', 'churchope' ) )
				->setId( SHORTNAME . '_new_label_color' )
				->setStd( '#ffffff' );
		$this->addOption( $option );

		$option = new Admin_Theme_Element_Colorchooser();
		$option->setName( __( 'New label background color', 'churchope' ) )
				->setDescription( __( 'New label background color', 'churchope' ) )
				->setId( SHORTNAME . '_new_label_bgcolor' )
				->setStd( '#c6320d' );
		$this->addOption( $option );

		$option = new Admin_Theme_Element_Separator();
		$this->addOption( $option );
		$option = null;

		$option = new Admin_Theme_Element_Checkbox();
		$option->setDescription( __( 'Hide Sale percentage product label', 'churchope' ) )
				->setName( __( 'Hide Sale percentage product label', 'churchope' ) )
				->setCustomized()
				->setId( SHORTNAME . '_sale_label' );
		$this->addOption( $option );
		$option = null;

		$option = new Admin_Theme_Element_Colorchooser();
		$option->setName( __( 'Sale label text color', 'churchope' ) )
				->setDescription( __( 'Sale label text color', 'churchope' ) )
				->setId( SHORTNAME . '_sale_label_color' )
				->setStd( '#ffffff' );
		$this->addOption( $option );

		$option = new Admin_Theme_Element_Colorchooser();
		$option->setName( __( 'Sale label background color', 'churchope' ) )
				->setDescription( __( 'Sale label background color', 'churchope' ) )
				->setId( SHORTNAME . '_sale_label_bgcolor' )
				->setStd( '#94b301' );
		$this->addOption( $option );

		$option = new Admin_Theme_Element_Separator();
		$this->addOption( $option );
		$option = null;

		$option = new Admin_Theme_Element_Checkbox();
		$option->setDescription( __( 'Hide Out of Stock product label', 'churchope' ) )
				->setName( __( 'Hide Out of Stock product label', 'churchope' ) )
				->setCustomized()
				->setId( SHORTNAME . '_outofstok_label' );
		$this->addOption( $option );
		$option = null;

		$option = new Admin_Theme_Element_Colorchooser();
		$option->setName( __( 'Out of Stock label text color', 'churchope' ) )
				->setDescription( __( 'Out of Stock label text color', 'churchope' ) )
				->setId( SHORTNAME . '_outofstok_label_color' )
				->setStd( '#ffffff' );
		$this->addOption( $option );

		$option = new Admin_Theme_Element_Colorchooser();
		$option->setName( __( 'Out of Stock label background color', 'churchope' ) )
				->setDescription( __( 'Out of Stock label background color', 'churchope' ) )
				->setId( SHORTNAME . '_outofstok_label_bgcolor' )
				->setStd( '#cfcfcf' );
		$this->addOption( $option );

		$option = new Admin_Theme_Element_Separator();
		$this->addOption( $option );
		$option = null;

		$option = new Admin_Theme_Element_Text();
		$option->setName( __( 'Widget Price font size', 'churchope' ) )
				->setDescription( __( 'Widget price font size at any units', 'churchope' ) )
				->setId( SHORTNAME . '_widget_font_size' )
				->setStd( '12px' )
				->setCustomized();
		$this->addOption( $option );
		$option = null;

		$option = new Admin_Theme_Element_Colorchooser();
		$option->setName( __( 'Widget Regular Price color', 'churchope' ) )
				->setDescription( __( 'Please select your custom color', 'churchope' ) )
				->setId( SHORTNAME . '_widget_pricecolor' )
				->setStd( '#c62c02' );
		$this->addOption( $option );

		$option = new Admin_Theme_Element_Colorchooser();
		$option->setName( __( 'Widget Sale Price color', 'churchope' ) )
				->setDescription( __( 'Please select your custom color.', 'churchope' ) )
				->setId( SHORTNAME . '_widget_special_pricecolor' )
				->setStd( '#758d02' );
		$this->addOption( $option );

		$option = new Admin_Theme_Element_Separator();
		$this->addOption( $option );
		$option = null;
	}

	/**
	 * Save form and set option-flag for reinit rewrite rules on init
	 */
	public function saveForm() {

		parent::saveForm();
		$this->setNeedReinitRulesFlag();
	}

	/**
	 * Reset form and set option-flag for reinit rewrite rules on init
	 */
	public function resetForm() {

		parent::resetForm();
		$this->setNeedReinitRulesFlag();
	}

	/**
	 * save to DB flag of need do flush_rewrite_rules on next init
	 */
	private function setNeedReinitRulesFlag() {

		update_option( SHORTNAME . '_need_flush_rewrite_rules', '1' );
	}
}

?>
