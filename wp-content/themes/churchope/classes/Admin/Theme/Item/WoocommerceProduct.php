<?php

/**
 * 'WooCommerce' admin menu page
 */
class Admin_Theme_Item_WoocommerceProduct extends Admin_Theme_Menu_Item
{

	/**
	 * prefix of file icons option
	 */
	public function __construct( $parent_slug = '' ) {

		$this->setPageTitle( __( 'WooCommerce Product Page options', 'churchope' ) );
		$this->setMenuTitle( __( 'WooCommerce Product', 'churchope' ) );
		$this->setCapability( 'administrator' );
		$this->setMenuSlug( SHORTNAME . '_woocommerce_product' );
		$this->setIsCustomize( true );
		parent::__construct( $parent_slug );
		$this->init();
	}

	public function init() {

		$option = new Admin_Theme_Element_Pagetitle();
		$option->setName( __( 'WooCommerce Product Page options', 'churchope' ) );
		$this->addOption( $option );
		$option = null;

		$option = new Admin_Theme_Element_Select();
		$option->setName( __( 'Sidebar position for single product page', 'churchope' ) )
				->setDescription( __( 'Choose a sidebar position for single product page', 'churchope' ) )
				->setId( SHORTNAME . '_product_layout' )
				->setStd( 'none' )
				->setOptions( array( 'none', 'left', 'right' ) );
		$this->addOption( $option );
		$option = null;

		$option = new Admin_Theme_Element_SelectSidebar();
		$option->setName( __( 'Sidebar for single product page', 'churchope' ) )
				->setDescription( __( 'Choose a sidebar for single product page', 'churchope' ) )
				->setId( SHORTNAME . '_product_sidebar' );
		$this->addOption( $option );
		$option = null;

		$option = new Admin_Theme_Element_Separator();
		$this->addOption( $option );
		$option = null;

		$option = new Admin_Theme_Element_Text();
		$option->setName( __( 'Single Product Price font size', 'churchope' ) )
				->setDescription( __( 'Widget price size at any units', 'churchope' ) )
				->setId( SHORTNAME . '_product_font_size' )
				->setStd( '26px' );
		$this->addOption( $option );
		$option = null;

		$option = new Admin_Theme_Element_Text();
		$option->setName( __( 'Single Product Price font weight', 'churchope' ) )
				->setDescription( __( 'Widget price font weight at any units', 'churchope' ) )
				->setId( SHORTNAME . '_product_font_weight' )
				->setStd( '400' );
		$this->addOption( $option );
		$option = null;

		$option = new Admin_Theme_Element_Colorchooser();
		$option->setName( __( 'Single Product Regular Price color', 'churchope' ) )
				->setDescription( __( 'Please select your custom color', 'churchope' ) )
				->setId( SHORTNAME . '_product_pricecolor' )
				->setStd( '#545454' );
		$this->addOption( $option );

		$option = new Admin_Theme_Element_Colorchooser();
		$option->setName( __( 'Single Product Sale Price color', 'churchope' ) )
				->setDescription( __( 'Please select your custom color.', 'churchope' ) )
				->setId( SHORTNAME . '_product_special_pricecolor' )
				->setStd( '#545454' );
		$this->addOption( $option );

		$option = new Admin_Theme_Element_Separator();
		$this->addOption( $option );
		$option = null;

		$option = new Admin_Theme_Element_Colorchooser();
		$option->setName( __( '"In stock" label color', 'churchope' ) )
				->setDescription( __( 'Please select your custom color.', 'churchope' ) )
				->setId( SHORTNAME . '_product_instock_color' )
				->setStd( '#758d02' );
		$this->addOption( $option );

		$option = new Admin_Theme_Element_Colorchooser();
		$option->setName( __( '"Out of stock" label color', 'churchope' ) )
				->setDescription( __( 'Please select your custom color.', 'churchope' ) )
				->setId( SHORTNAME . '_product_outofstock_color' )
				->setStd( '#c62c02' );
		$this->addOption( $option );
	}

	/**
	 * Save form and set option-flag for reinit rewrite rules on init
	 */
	public function saveForm() {

		parent::saveForm();
		$this->setNeedReinitRulesFlag();
	}

	/**
	 * Reset form and set option-flag for reinit rewrite rules on init
	 */
	public function resetForm() {

		parent::resetForm();
		$this->setNeedReinitRulesFlag();
	}

	/**
	 * save to DB flag of need do flush_rewrite_rules on next init
	 */
	private function setNeedReinitRulesFlag() {

		update_option( SHORTNAME . '_need_flush_rewrite_rules', '1' );
	}
}

?>
