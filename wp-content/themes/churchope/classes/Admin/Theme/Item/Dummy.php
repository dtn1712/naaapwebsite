<?php
/**
 * 'General' admin menu page
 */
class Admin_Theme_Item_Dummy extends Admin_Theme_Menu_Item
{



	public function __construct( $parent_slug = '' ) {

		$color = (get_option( SHORTNAME . '_dummy_install' ))? 'opacity:0.3':'color:#00a0c6';

		$this->setPageTitle( __( 'Dummy content','churchope' ) );
		$this->setMenuTitle( '<span style="'.$color.'">'.__( 'Dummy content','churchope' ).'</span>' );
		$this->setCapability( 'administrator' );
		$this->setMenuSlug( SHORTNAME.'_dummy' );
		$this->setIsCustomize( false );
		parent::__construct( $parent_slug );
		$this->init();
	}

	public function init() {

		$option = new Admin_Theme_Element_Pagetitle();
		$option->setName( __( 'Dummy content install','churchope' ) );
		$this->addOption( $option );
		$option = null;

		$option = new Admin_Theme_Element_Radio_Dummy();
		$option->setName( 'Use type of install' )
				->setDescription( 'You can install different dummy content' )
				->setId( SHORTNAME . '_dummy_type' )
				->setStd( Import_Dummy::V1 )
				->setOptions(array(
					Import_Dummy::V1 => 'Default dummy',
					Import_Dummy::V2 => 'Modern dummy',
				));
		$this->addOption( $option );
		$option = null;

		$option = new Admin_Theme_Element_InstallDummy();
		$this->addOption( $option );

	}
}
?>
