<?php
$file_handle = fopen(get_template_directory() . '/old_member_list.csv', 'r');
while (!feof($file_handle) ) {
	$line_of_text[] = fgetcsv($file_handle, 1024);
}
fclose($file_handle);

unset($line_of_text[0]);

global $wpdb;

foreach($line_of_text as $item)
{
	$first_name = $item[0];
	$last_name = $item[1];
	$membership_expired = $item[2];
	$email = $item[4];
	$created_date = $item[5];
	$user_name = $item[6];
	$display_name = $item[7];
	$password = $item[8];
	$street = $item[9];
	$city = $item[10];
	$state = $item[11];
	$zip_code = $item[12];
	$phone = $item[13];

	if(!empty($email) || !empty($user_name))
	{
		$email = empty($email) ? 'please_change_to_your_email_' . uniqid() . '@email.com' : $email;
		$user_name = empty($user_name) ? $email : $user_name;
		$password = empty($password) ? rand(100, 1000000) . '_random_' . uniqid() : $password;
		$is_active_status = $membership_expired == 'Expired' ? FALSE : TRUE;
		$display_name = empty($display_name) ? $user_name : $display_name;

		if(empty($created_date))
		{
			$created_date = date('Y-m-d', time());
		}
		else
		{
			$created_date = date('Y-m-d', strtotime($created_date));
		}

		if(empty(username_exists($email)))
		{
			$user_info = array(
				'user_login' => $user_name,
				'user_pass' => wp_hash_password($password),
				'user_nicename' => $display_name,
				'user_email' => $email,
				'user_url' => '',
				'user_registered' => $created_date,
				'user_activation_key' => '',
				'user_status' => 0,
				'display_name' => $display_name
			);

			$wpdb->insert($wpdb->prefix . 'users', $user_info);

			$user_id = $wpdb->insert_id;

			if(!empty($user_id))
			{
				if($is_active_status)
				{
					$user = new WP_User( $user_id );
					$user->set_role( 'subscriber' );

					$user_resume_data = array(
						'user_id' => $user_id,
						'candidate_slug' => sanitize_title($first_name . ' ' . $last_name),
						'phone' => $phone,
						'created_at' => $created_date,
						'modified_at' => $created_date,
						'candidate_country' => 840,
						'candidate_state' => $state,
						'candidate_zip_code' => $zip_code,
						'is_active' => 1,
						'is_public' => 1,
					);

					$wpdb->insert($wpdb->prefix . 'wpjb_resume', $user_resume_data);

					$user_resume_search_data = array(
						'resume_id' => $wpdb->insert_id,
						'fullname' => $first_name . ' ' . $last_name,
						'location' => 'US USA United States',
						'details' => '',
						'details_all' => '',
					);

					$wpdb->insert($wpdb->prefix . 'wpjb_resume_search', $user_resume_search_data);
				}

				$user_meta = array(
					'first_name' => $first_name,
					'last_name' => $last_name,
					'nickname' => $display_name,
					'address_line_1' => $street,
					'address_city' => $city,
					'address_state' => $state,
					'address_zipcode' => $zip_code,
					'contact_phone_mobile' => $phone
				);

				foreach($user_meta as $key => $meta_data)
				{
					if(!empty($meta_data))
					{
						$insert_user_meta = array(
							'user_id' => $user_id,
							'meta_key' => $key,
							'meta_value' => $meta_data,
						);

						$wpdb->insert($wpdb->prefix . 'usermeta', $insert_user_meta);
					}
				}
			}
		}
	}
}
