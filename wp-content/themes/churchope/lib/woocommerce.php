<?php
/**
 * Custom WooCommerce functions that act independently of the theme templates
 *
 * Eventually, some of the functionality here could be replaced by core features
 *
 * @package Churchope
 */
// Declare WooCommerce support
add_theme_support( 'woocommerce' );

global $labels_round_style;

$labels_round_style = (get_option( SHORTNAME . '_round_labels' )) ? 'round' : '';

// Display the new badge
if ( ! function_exists( 'ox_woocommerce_new_badge' ) ) {
	function ox_woocommerce_new_badge() {

		global $labels_round_style;
		$postdate = get_the_time( 'Y-m-d' );
		$postdatestamp = strtotime( $postdate );
		$newness = (get_option( SHORTNAME . '_new_label_days' )) ? get_option( SHORTNAME . '_new_label_days' ) : '30';

		if ( ( time() - ( 60 * 60 * 24 * $newness ) ) < $postdatestamp ) {
			echo '<span class="product-badge new ' . $labels_round_style . '">' . __( 'New', 'churchope' ) . '</span>';
		}
	}

	if ( ! (get_option( SHORTNAME . '_new_label' )) ) {
		add_action( 'woocommerce_after_shop_loop_product_thumbnail', 'ox_woocommerce_new_badge', 4 );
		add_action( 'woocommerce_after_single_product_main_images', 'ox_woocommerce_new_badge', 4 );
	}
}

// Display the slod out badge
if ( ! function_exists( 'ox_woocommerce_sold_badge' ) ) {
	function ox_woocommerce_sold_badge() {

		global $product, $labels_round_style;
		if ( ! $product->is_in_stock() ) {
			echo '<span class="product-badge sold ' . $labels_round_style . '">' . __( 'Sold Out', 'churchope' ) . '</span>';
		}
	}

	if ( ! (get_option( SHORTNAME . '_outofstok_label' )) ) {
		add_action( 'woocommerce_after_shop_loop_product_thumbnail', 'ox_woocommerce_sold_badge', 3 );
		add_action( 'woocommerce_after_single_product_main_images', 'ox_woocommerce_sold_badge', 3 );
	}
}



// Display the percentage sale badge
if ( ! function_exists( 'ox_woocommerce_percentage_sale_badge' ) ) {
	function ox_woocommerce_percentage_sale_badge() {

		global $product, $labels_round_style;

		$percentage = 0;

		if ( $product->product_type == 'variable' ) {
			$available_variations = $product->get_available_variations();

			for ( $i = 0; $i < count( $available_variations ); ++$i ) {
				$variation_id = $available_variations[ $i ]['variation_id'];
				$variable_product = new WC_Product_Variation( $variation_id );
				$regular_price = $variable_product->regular_price;
				$sales_price = $variable_product->sale_price;
				if ( $sales_price ) {
					$variable_product_percentage = round( (( ( $regular_price - $sales_price ) / $regular_price ) * 100) );
					if ( $variable_product_percentage > $percentage ) {
						$percentage = $variable_product_percentage;
					}
				}
			}
		} else {
			$percentage = round( ( ( $product->regular_price - $product->sale_price ) / $product->regular_price ) * 100 );
		}
			$text = __( 'Sale', 'churchope' );
		if ( ! (get_option( SHORTNAME . '_sale_label' )) ) {
			$text = $percentage . __( '% OFF', 'churchope' );
		}

			return '<span class="product-badge sale ' . $labels_round_style . '">' . $text . '</span>';
	}

	add_filter( 'woocommerce_sale_flash', 'ox_woocommerce_percentage_sale_badge' );
}


// Remove woo lightbox
add_action( 'wp_enqueue_scripts', 'ox_remove_woo_lightbox', 99 );

function ox_remove_woo_lightbox() {

	remove_action( 'wp_head', array( $GLOBALS['woocommerce'], 'generator' ) );
	wp_dequeue_style( 'woocommerce_prettyPhoto_css' );
	wp_dequeue_script( 'prettyPhoto' );
	wp_dequeue_script( 'prettyPhoto-init' );
}

if ( ! function_exists( 'ox_woocommerce_catalog_per_page' ) ) {
	/**
	 * Output the product per page options.
	 *
	 * @subpackage	Loop
	 */
	function ox_woocommerce_catalog_per_page() {

		global $wp_query;

		if ( 1 == $wp_query->found_posts || ! woocommerce_products_will_display() ) {
			return;
		}

		$default_products_per_page = (get_option( SHORTNAME . '_woo_listing_per_page' )) ? get_option( SHORTNAME . '_woo_listing_per_page' ) : get_option( 'posts_per_page' );

		$products_per_page = isset( $_COOKIE['products_per_page'] ) ? wc_clean( $_COOKIE['products_per_page'] ) : apply_filters( 'woocommerce_default_catalog_products_per_page', $default_products_per_page );

		$catalog_products_per_page_options = apply_filters('woocommerce_catalog_products_per_page', array(
			$default_products_per_page => sprintf( __( 'Show %1$d per page', 'churchope' ), $default_products_per_page ),
			$default_products_per_page * 2 => sprintf( __( 'Show %1$d per page', 'churchope' ), $default_products_per_page * 2 ),
			$default_products_per_page * 3 => sprintf( __( 'Show %1$d per page', 'churchope' ), $default_products_per_page * 3 ),
			'-1' => __( 'Show all products', 'churchope' ),
		));
		?>
        <form class="woocommerce-per-page" method="get">
            <select name="products_per_page" class="products_per_page">
				<?php
				foreach ( $catalog_products_per_page_options as $id => $name ) :

					if ( $id <= $wp_query->found_posts || $id == $default_products_per_page ) {
					?>
						<option value="<?php echo esc_attr( $id ); ?>" <?php selected( $products_per_page, $id ); ?>><?php echo esc_html( $name ); ?></option>
						<?php
					}

				endforeach;
				?>
            </select>
			<?php
			// Keep query string vars intact
			foreach ( $_GET as $key => $val ) {
				if ( 'products_per_page' === $key || 'submit' === $key ) {
					continue;
				}
				if ( is_array( $val ) ) {
					foreach ( $val as $innerVal ) {
						echo '<input type="hidden" name="' . esc_attr( $key ) . '[]" value="' . esc_attr( $innerVal ) . '" />';
					}
				} else {
					echo '<input type="hidden" name="' . esc_attr( $key ) . '" value="' . esc_attr( $val ) . '" />';
				}
			}
			?>
        </form>

		<?php
	}

	if ( ! get_option( SHORTNAME . '_woo_per_page_filter' ) ) {
		add_action( 'woocommerce_before_shop_loop', 'ox_woocommerce_catalog_per_page', 31 );
	}


	add_filter( 'loop_shop_per_page', 'ox_loop_catalog_per_page' );

	function ox_loop_catalog_per_page() {

		return $products_per_page = isset( $_COOKIE['products_per_page'] ) ? wc_clean( $_COOKIE['products_per_page'] ) : apply_filters( 'woocommerce_default_catalog_products_per_page', get_option( SHORTNAME . '_woo_listing_per_page' ) );
	}
}

// Product listing thumb
function woocommerce_get_product_thumbnail( $size = 'churchope_shop_catalog' ) {

	global $post;
	$shop_catalog_image = get_option( 'shop_catalog_image_size', array() );
	$width = isset( $shop_catalog_image['width'] ) ? $shop_catalog_image['width'] : '300';
	$height = isset( $shop_catalog_image['height'] ) ? $shop_catalog_image['height'] : '300';

	if ( has_post_thumbnail() ) {
		ob_start();
		get_theme_post_thumbnail( $post->ID, $size );
		$html = ob_get_clean();
		return $html;
	} elseif ( woocommerce_placeholder_img_src() ) {
		return '<img src="' . woocommerce_placeholder_img_src() . '" alt="' . __( 'Placeholder', 'churchope' ) . '" width="' . $width . '" height="' . $height . '"  class="img_placeholder" />';
	}
}

// Category thumb
function woocommerce_subcategory_thumbnail( $category ) {

	$dimensions = wc_get_image_size( 'churchope_shop_catalog' );

	$thumbnail_id = get_woocommerce_term_meta( $category->term_id, 'thumbnail_id', true );

	if ( $thumbnail_id ) {
		$image_obj = theme_post_thumbnail_src( $thumbnail_id, 'churchope_shop_catalog' );
		$image = $image_obj[0];
	} else {
		$image = woocommerce_placeholder_img_src();
	}
	if ( $image ) {
		echo '<img src="' . $image . '" alt="' . $category->name . '" width="' . $dimensions['width'] . '" height="' . $dimensions['height'] . '" />';
	}
}

// Product widgets thumb
function woocommerce_widgets_thumbnails( $size = 'churchope_shop_widget' ) {

	global $post;

	$image = '';

	if ( has_post_thumbnail( $post->ID ) ) {
		ob_start();
		get_theme_post_thumbnail( $post->ID, $size );
		$image = ob_get_clean();
	} elseif ( ( $parent_id = wp_get_post_parent_id( $post->ID ) ) && has_post_thumbnail( $parent_id ) ) {
		ob_start();
		get_theme_post_thumbnail( $parent_id, $size );
		$image = ob_get_clean();
	} else {
		$image = '<img src="' . woocommerce_placeholder_img_src() . '" alt="' . __( 'Placeholder', 'churchope' ) . '"  class="img_placeholder" />';
	}

	return $image;
}

// TopLine login
function th_shop_login() {

	if ( is_user_logged_in() ) {
	?>
		<a href="<?php echo wc_get_page_permalink( 'myaccount' ); ?>" title="<?php _e( 'My Account', 'churchope' ); ?>"><?php _e( 'My Account', 'churchope' ); ?></a><span class="topline-divider"></span><a href="<?php echo wc_get_endpoint_url( 'customer-logout', '', wc_get_page_permalink( 'myaccount' ) ); ?>" title="<?php _e( 'Logout', 'churchope' ); ?>"><?php _e( 'Logout', 'churchope' ); ?></a>
		<?php
	} else {
		?>
		<a href="<?php echo get_permalink( get_option( 'woocommerce_myaccount_page_id' ) ); ?>" title="<?php _e( 'Login', 'churchope' ); ?><?php ( get_option( 'woocommerce_enable_myaccount_registration' ) === 'yes' ) ? _e( ' / Register', 'churchope' ) : ''; ?>"><?php _e( 'Login', 'churchope' ); ?><?php ( get_option( 'woocommerce_enable_myaccount_registration' ) === 'yes' ) ? _e( ' / Register', 'churchope' ) : ''; ?></a>
		<?php
	}
}

// My account user info
function woocommerce_my_account_user_info() {

	global $post;
	if ( (is_account_page() || $post->post_parent == wc_get_page_id( 'myaccount' )) && is_user_logged_in() ) {
		global $current_user;
		$html = '<div class="widget woocommerce widget-user-myaccount">';
		$html .= '<a class="avatar-myaccount" href="' . wc_get_page_permalink( 'myaccount' ) . '" >' . get_avatar( $current_user->user_email, $size = '45' ) . '</a>';
		$html .= '<h2><a href="' . wc_get_page_permalink( 'myaccount' ) . '" >' . $current_user->display_name . '</a></h2>';
		$html .= '<a href="' . wc_get_endpoint_url( 'customer-logout', '', wc_get_page_permalink( 'myaccount' ) ) . '" title="' . __( 'Logout', 'churchope' ) . '" class="sidebar-user-logout">' . __( 'Logout', 'churchope' ) . '</a>';
		$html .= '</div>';

		echo $html;
	}
	return;
}

function woocommerce_header_add_to_cart_fragment( $fragments ) {

	global $woocommerce;
	ob_start();
	?>

	<span class="top_cart_text ribbon"><span><?php echo sprintf( __( '%1$d', 'churchope' ), $woocommerce->cart->cart_contents_count ); ?></span></span>

	<?php
	$fragments['span.top_cart_text'] = ob_get_clean();
	return $fragments;
}

// Hooks
add_filter( 'add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment' );

add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );
add_filter( 'woocommerce_show_page_title', '__return_empty_array' );

remove_action( 'woocommerce_cart_collaterals', 'woocommerce_cart_totals', 10 );

remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0 );
remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10 );
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10 );
remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10 );
remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 10 );
remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash', 10 );
remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 );
remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10 );

add_action( 'woocommerce_after_single_product_main_images', 'woocommerce_show_product_sale_flash', 10 );
add_action( 'woocommerce_after_shop_loop_product_thumbnail', 'woocommerce_show_product_loop_sale_flash', 10 );
add_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 );
add_action( 'woocommerce_shop_loop_item_image', 'woocommerce_template_loop_product_thumbnail', 10 );
add_action( 'woocommerce_title_breadcrumb', 'woocommerce_breadcrumb', 20, 0 );


remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
add_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 31 );

remove_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10 );
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 5 );
remove_action( 'woocommerce_before_subcategory', 'woocommerce_template_loop_category_link_open', 10 );
remove_action( 'woocommerce_after_subcategory', 'woocommerce_template_loop_category_link_close', 10 );

add_action( 'th_sidebar_before', 'woocommerce_my_account_user_info', 10 );


if ( get_option( SHORTNAME . '_woo_rating' ) ) {
	remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 );
}

if ( get_option( SHORTNAME . '_woo_add_to_cart' ) ) {
	remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );
}
